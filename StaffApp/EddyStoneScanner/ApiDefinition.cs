﻿using System;

using UIKit;
using Foundation;
using ObjCRuntime;
using CoreGraphics;
using CoreBluetooth;

namespace EddyStoneScanner
{
	// @protocol ESSBeaconScannerDelegate <NSObject>
	[Protocol, Model]
	[BaseType(typeof(NSObject))]
	interface ESSBeaconScannerDelegate
	{
		// @optional -(void)beaconScanner:(ESSBeaconScanner *)scanner didFindBeacon:(id)beaconInfo;
		[Export("beaconScanner:didFindBeacon:")]
		void DidFindBeacon(ESSBeaconScanner scanner, NSObject beaconInfo);

		// @optional -(void)beaconScanner:(ESSBeaconScanner *)scanner didLoseBeacon:(id)beaconInfo;
		[Export("beaconScanner:didLoseBeacon:")]
		void DidLoseBeacon(ESSBeaconScanner scanner, NSObject beaconInfo);

		// @optional -(void)beaconScanner:(ESSBeaconScanner *)scanner didUpdateBeacon:(id)beaconInfo;
		[Export("beaconScanner:didUpdateBeacon:")]
		void DidUpdateBeacon(ESSBeaconScanner scanner, NSObject beaconInfo);

		// @optional -(void)beaconScanner:(ESSBeaconScanner *)scanner didFindURL:(NSURL *)url;
		[Export("beaconScanner:didFindURL:")]
		void DidFindURL(ESSBeaconScanner scanner, NSUrl url);
	}

	// @interface ESSBeaconScanner : NSObject
	[BaseType(typeof(NSObject))]
	interface ESSBeaconScanner
	{
		[Wrap("WeakDelegate")]
		ESSBeaconScannerDelegate Delegate { get; set; }

		// @property (nonatomic, weak) id<ESSBeaconScannerDelegate> delegate;
		[NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
		NSObject WeakDelegate { get; set; }

		// @property (assign, nonatomic) NSTimeInterval onLostTimeout;
		[Export("onLostTimeout")]
		double OnLostTimeout { get; set; }

		// -(void)startScanning;
		[Export("startScanning")]
		void StartScanning();

		// -(void)stopScanning;
		[Export("stopScanning")]
		void StopScanning();
	}

	// @interface ESSBeaconID : NSObject <NSCopying>
	[BaseType(typeof(NSObject))]
	interface ESSBeaconID : INSCopying
	{
		// @property (readonly, assign, nonatomic) ESSBeaconType beaconType;
		[Export("beaconType", ArgumentSemantic.Assign)]
		ESSBeaconType BeaconType { get; }

		// @property (readonly, copy, nonatomic) NSData * beaconID;
		[Export("beaconID", ArgumentSemantic.Copy)]
		NSData BeaconID { get; }
	}

	// @interface ESSBeaconInfo : NSObject
	[BaseType(typeof(NSObject))]
	interface ESSBeaconInfo
	{
		// @property (readonly, nonatomic, strong) NSNumber * RSSI;
		[Export("RSSI", ArgumentSemantic.Strong)]
		NSNumber RSSI { get; }

		// @property (readonly, nonatomic, strong) ESSBeaconID * beaconID;
		[Export("beaconID", ArgumentSemantic.Strong)]
		ESSBeaconID BeaconID { get; }

		// @property (readonly, copy, nonatomic) NSData * telemetry;
		[Export("telemetry", ArgumentSemantic.Copy)]
		NSData Telemetry { get; }

		// @property (readonly, nonatomic, strong) NSNumber * txPower;
		[Export("txPower", ArgumentSemantic.Strong)]
		NSNumber TxPower { get; }

		// +(ESSFrameType)frameTypeForFrame:(NSData *)frameData;
		[Static]
		[Export("frameTypeForFrame:")]
		ESSFrameType FrameTypeForFrame(NSData frameData);

		// +(instancetype)beaconInfoForUIDFrameData:(NSData *)UIDFrameData telemetry:(NSData *)telemetry RSSI:(NSNumber *)initialRSSI;
		[Static]
		[Export("beaconInfoForUIDFrameData:telemetry:RSSI:")]
		ESSBeaconInfo BeaconInfoForUIDFrameData(NSData UIDFrameData, NSData telemetry, NSNumber initialRSSI);

		// +(instancetype)beaconInfoForEIDFrameData:(NSData *)EIDFrameData telemetry:(NSData *)telemetry RSSI:(NSNumber *)initialRSSI;
		[Static]
		[Export("beaconInfoForEIDFrameData:telemetry:RSSI:")]
		ESSBeaconInfo BeaconInfoForEIDFrameData(NSData EIDFrameData, NSData telemetry, NSNumber initialRSSI);

		// +(NSURL *)parseURLFromFrameData:(NSData *)URLFrameData;
		[Static]
		[Export("parseURLFromFrameData:")]
		NSUrl ParseURLFromFrameData(NSData URLFrameData);

		// +(CBUUID *)eddystoneServiceID;
		[Static]
		[Export("eddystoneServiceID")]
		CBUUID EddystoneServiceID { get; }

		// +(ESSBeaconInfo *)testBeaconFromBeaconIDString:(NSString *)beaconID;
		[Static]
		[Export("testBeaconFromBeaconIDString:")]
		ESSBeaconInfo TestBeaconFromBeaconIDString(string beaconID);
	}


}
