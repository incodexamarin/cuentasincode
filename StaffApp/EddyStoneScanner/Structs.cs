﻿using System;
using ObjCRuntime;

namespace EddyStoneScanner
{
[Native]
public enum ESSBeaconType : long
{
	kESSBeaconTypeEddystone = 1,
	Eid = 2
}

[Native]
public enum ESSFrameType : long
{
	UnknownFrameType = 0,
	UIDFrameType,
	URLFrameType,
	EIDFrameType,
	TelemetryFrameType	}
}
