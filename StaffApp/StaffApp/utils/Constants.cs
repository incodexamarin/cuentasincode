﻿using System;

namespace Knetik_Rest
{

	public class Constants
	{

		public static string SECRET_KEY = "superSUPERsuperSECRET";
		public static string CLIENT_KEY = "knetik";
		public static string ClIENT_ID = "knetik_web";

		////----------------------   STAGING _SERVER ------------------------//


		//public static string KNETIK_JSPAI_URL = "https://kniot.sandbox.knetikcloud.com";
		//public static string KNETIK_NOTIFICATION_URL = "https://kne.kniot.admin.sandbox.knetikcloud.com:8000";
		//public static string KNETIK_SOCKET_URL = "https://kne.kniot.admin.sandbox.knetikcloud.com:14000";



		////----------------------   DEV _SERVER ------------------------//


		public static string KNETIK_JSPAI_URL = "https://iot.devsandbox.knetikcloud.com";
		public static string KNETIK_NOTIFICATION_URL = "https://iot.kne.devsandbox.knetikcloud.com";
		public static string KNETIK_SOCKET_URL = "https://iot.kne.devsandbox.knetikcloud.com:14000";


	}
}

