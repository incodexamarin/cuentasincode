﻿using System;
using System.Threading.Tasks;

namespace KnetikIOT
{
	public class Util
	{
		public Util()
		{
		}

		public static IProximity GetProximityFromDistance(double distance) {

			if (distance > 0 && distance <3)
			{

				return IProximity.NEAR;
			}
			else
			if (distance > 3 && distance< 6) { 
				
					return IProximity.MEDUIM;
			}
			else
			if (distance > 6 ) { 
				
				return IProximity.FAR;
			}
			return IProximity.LOST;
		
		}
	}
}
