﻿using System;
using System.Collections.Generic;
using JSAPISdk;

namespace KnetikIOT
{
	public interface ILocationHasChanged
	{
		void locationChanged(Location location);
		void displayAlertMessage(string message);
	}
}
