﻿using System;
using System.Collections.Generic;
using JSAPISdk;

namespace KnetikIOT
{
	public interface IBeaconStatusChanged
	{
		void becaonStatus(UserDevice device, IProximity proximity);
		void updateBeaconsList(IList<BeaconDevice> devices);
		void onEnterRegion(BeaconDevice beaconDevice);
		void onExitRegion(BeaconDevice beaconDevice);
		void displayAlertMessage(string message);
        void devicePositionChanged(double x, double y, double z);

	}
}
