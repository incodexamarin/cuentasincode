﻿using System;

namespace KnetikIOT
{
	public interface IEddyStone
	{
		void RegisterEddystoneDelegate(IEddyStoneStatusChanged beaconStatus);
		void VerityBluetooth();
		void NotifyServiceConnection();
		void OnstopSearch();
		void OnResumeSearch();
		void OnBackGroundSearch();
		void OnPauseSearch();
		void OnClose();
	}
}
