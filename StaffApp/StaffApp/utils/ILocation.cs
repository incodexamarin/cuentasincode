﻿using System;
using JSAPISdk;

namespace KnetikIOT
{
	public interface ILocation
	{
		void registerLocationDelegate(ILocationHasChanged locationM);
		void verifyLocation();
		void onLocationResume();
		void RemoveUpdates();
		void OnConnected();
		Location getCurrentLocation(); 
		double DistanceBetween(double startLatitude, double startLongitude, double endLatitude, double endLongitude);
	}
}
