﻿using System;
namespace KnetikIOT
{
	public interface FacebookLoginCompleted
	{
		void facebookCompleted(string token);
		void facebookError(string error);
	}
}
