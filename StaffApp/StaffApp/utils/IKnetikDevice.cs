﻿using System;
using JSAPISdk;

namespace KnetikIOT
{
	public interface IKnetikDevice
	{

		UserDevice getUserDevice();
	}
}
