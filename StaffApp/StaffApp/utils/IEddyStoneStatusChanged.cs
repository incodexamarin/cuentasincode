﻿using System;
using System.Collections.Generic;
using JSAPISdk;

namespace KnetikIOT
{
public interface IEddyStoneStatusChanged
	{
		void EddyStoneStatus(UserDevice device, IProximity proximity);
		void updateEddyStonesList(IList<BeaconDevice> devices);
		void onEnterRegion(BeaconDevice EddyStoneDevice);
		void onExitRegion(BeaconDevice EddyStoneDevice);
		void displayAlertMessage(string message);
	}
}
