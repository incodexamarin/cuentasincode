﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;

namespace Knetik_Rest
{
	public class ChallengesRepository
	{
		private static ChallengesRepository instance;

		public static Object lockObject = new object();

		Dictionary<String, ChallengeResponse> challengeResponse;

		public static ChallengesRepository Instance {

			get
			{

				if (instance == null)
				{
					lock(lockObject) {

						if (instance == null)
						{
							instance = new ChallengesRepository();
							instance.challengeResponse = new Dictionary<string, ChallengeResponse>();
						}
					}


				}

				return instance;
			}
		}


		private ChallengesRepository()
		{
		}

		public void GetChallenges(ChallengeDelegate challengeDelegate) {

			CampaignService.Instance.GetChallenges(challengeDelegate);

		}


		public void GetChallengeActivityById(String challengeId,ChallengeActivityDelegate challengeActivity)
		{

			CampaignService.Instance.GetChallengeActivityByChallengeId(challengeId,challengeActivity);

		}


		public void GetChallengeEventById(String challengeId, ChallengeEventDelegate challengeEvent)
		{

			CampaignService.Instance.GetChallengeEventByChallengeId(challengeId, challengeEvent);

		}
	
		public void CheckIfVideoHasChallenge(String videoId, SocketChallengeDelegate socketChallenge)
		{

			CampaignService.Instance.CheckIfVideoHasChallenge(videoId, socketChallenge);

		}

	}
}

