﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;
using JSAPISdk;
using Xamarin.Forms;
using KnetikIOT;

namespace Knetik_Rest
{
	public class UserRepository
	{
		private static UserRepository instance;

		private KnetikUser user;
		public static Object lockObject = new object();
		private string userLatitdue;
		private string userLongitude;
		private bool insideUpdateLocation;
		public static UserRepository Instance
		{

			get
			{

				if (instance == null)
				{
					lock (lockObject)
					{

						if (instance == null)
						{
							instance = new UserRepository();
						}
					}


				}

				return instance;
			}
		}


		private UserRepository()
		{
		}

		public void registerANewUser(KnetikUser user, UserDelegate userDelegate)
		{

			UserService.Instance.registerUser(user, userDelegate);

		}

		public void GetUserInfo(UserDelegate userDelegate)
		{
			if (user != null)
			{

				userDelegate(user, "", true);
				return;
			}

			UserService.Instance.GetUserInfo((KnetikUser result, string errorMessage, bool isSuccess) =>
			{

				if (isSuccess && result != null && result.userId > 0)
				{

					user = result;
				}
				userDelegate(user, errorMessage, isSuccess);


			});

		}


		public void GetUserInfoById(string userId,UserDelegate userDelegate)
		{

			UserService.Instance.GetUserInfoById(userId, (KnetikUser result, string errorMessage, bool isSuccess) =>
			 {

				 userDelegate(result, errorMessage, isSuccess);


			 });

		}


		public void UpdateUserInfo(KnetikUser user, UserDelegate userDelegate)
		{

			UserService.Instance.UpdateUserDetails(user, userDelegate);

		}


		public void updateUserLocations(string latitude, string longitude, UserDelegate userDelegate)
		{
			if (insideUpdateLocation)
				return;
			if (latitude == null || longitude == null)
				return;

			if (userLatitdue != null && userLongitude != null)
			{

				if (latitude == userLatitdue && longitude == userLongitude)
				{
					return;
				}
			}
			insideUpdateLocation = true;
			GetUserInfo((KnetikUser user, string nerrorMessage, bool isSuccess) =>
			{

				if (!isSuccess || user.username == null)
				{
					insideUpdateLocation = false;
					return;
				}

				Latitude lat = new Latitude();
				lat.value = latitude;
				lat.type = "text";

				Longitude lang = new Longitude();
				lang.value = longitude;
				lang.type = "text";
				user.additional_properties = new UserAdditionalProperties();
				user.additional_properties.latitude = lat;
				user.additional_properties.longitude = lang;
				UserDevice device = DependencyService.Get<IKnetikDevice>().getUserDevice();
				Token token = new Token();
				token.type = "text";
				token.value = device.data.token;
				user.additional_properties.Token = token;

				UserRepository.Instance.UpdateUserInfo(user, (KnetikUser newUser, string errorMessage, bool nisSuccess) =>
				{
					if (nisSuccess)
					{
						userLatitdue = latitude;
						userLongitude = longitude;
					}
					insideUpdateLocation = false;
					GetUserInfo(userDelegate);
				});

			});

		}


		public void updateUserToken(UserDelegate userDelegate)
		{
			GetUserInfo((KnetikUser user, string nerrorMessage, bool isSuccess) =>
			{

				if (!isSuccess || user.username == null)
				{
					return;
				}
				if (user.additional_properties == null)
					user.additional_properties = new UserAdditionalProperties();

				UserDevice device = DependencyService.Get<IKnetikDevice>().getUserDevice();
				Token token = new Token();
				token.type = "text";
				token.value = device.data.token;
				user.additional_properties.Token = token;

				UserRepository.Instance.UpdateUserInfo(user, (KnetikUser newUser, string errorMessage, bool nisSuccess) =>
				{
					GetUserInfo(userDelegate);
				});

			});
		}



		public void LoginWithGoogleAccount(String email, String givenName, String userId, String avatarUrl, LoginDelegate loginDelegate)
		{

			KnetikUser user = new KnetikUser();

			user.avatar_url = avatarUrl;
			user.email = email;
			user.username = email;
			user.password = GeneratePasswordFromEmail(email);

			if (givenName == null)
				givenName = email;

			user.first_name = givenName;

			user.display_name = givenName;



			UserService.Instance.loginWithUser(user, (resultLogin, errorMessageLogin, isSuccessLogin) =>
			{

				if (isSuccessLogin)
				{

					loginDelegate(resultLogin, errorMessageLogin, isSuccessLogin);

				}
				else
				{

					registerANewUser(user, (reg_result, reg_errorMessage, reg_isSuccess) =>
					{
						if (reg_isSuccess)
						{

							LoginWithGoogleAccount(email, givenName, userId, avatarUrl, loginDelegate);

						}
						else
						{

							loginDelegate(null, reg_errorMessage, reg_isSuccess);
						}


					});
				}

			});

		}


		public void LoginWithUserNameAndPassword(String email, String password, LoginDelegate loginDelegate)
		{

			KnetikUser user = new KnetikUser();

			user.email = email;
			user.username = email;
			user.password = password;

			user.first_name = email;

			user.display_name = email;



			UserService.Instance.loginWithUser(user, (resultLogin, errorMessageLogin, isSuccessLogin) =>
			{

				loginDelegate(resultLogin, errorMessageLogin, isSuccessLogin);


			});

		}

		public void LoginWithFacebookAccount(String token, LoginDelegate loginDelegate)
		{


			UserService.Instance.loginWithFacebookUser(token, (resultLogin, errorMessageLogin, isSuccessLogin) =>
			{

				loginDelegate(resultLogin, errorMessageLogin, isSuccessLogin);


			});

		}

		public void getUserAchievement(AchievementsDelegate achievementsDelegate)
		{
			UserRepository.Instance.GetUserInfo((result, errorMessage, isSuccess) =>
			{

				AchievementsService.Instance.GetAchievementsService((AchievementsResponse achievmentResponse, string eMessage, bool isPass) =>
				{

					if (isPass)
					{

						AchievementsService.Instance.GetUserAchievementsService("" + result.userId, (userachievmentResponse, UerrorMessage, UisSuccess) =>
						{

							if (isSuccess)
							{

								IList<Achievement> userachievments = userachievmentResponse.achievements;

								IList<Achievement> achievments = achievmentResponse.achievements;

								for (int i = 0; i < userachievments.Count; i++)
								{

									for (int j = 0; j < achievments.Count; j++)
									{

										Achievement userAchievement = userachievments[i];

										Achievement mainchievement = achievments[j];

										if (userAchievement.AchievementName != null && mainchievement.name != null && userAchievement.AchievementName.Equals(mainchievement.name))
										{

											mainchievement.isEarned = true;
										}


									}


								}
								achievmentResponse.achievements = achievments;

								achievementsDelegate(achievmentResponse, UerrorMessage, UisSuccess);


							}
							else
							{

								achievementsDelegate(userachievmentResponse, UerrorMessage, UisSuccess);
							}


						});


					}
					else
					{

						achievementsDelegate(achievmentResponse, eMessage, isPass);

					}


				});

			});


		}

		public void getUserAchievementByName(String name, AchievementByNameDelegate achievementsDelegate)
		{
			UserRepository.Instance.GetUserInfo((result, errorMessage, isSuccess) =>
			{

				AchievementsService.Instance.GetAchievementByNameService(name, (achievmentResponse, eMessage, isPass) =>
				 {

					 if (isPass)
					 {
						 achievementsDelegate(achievmentResponse, eMessage, isPass);

					 }
					 else
					 {

						 achievementsDelegate(achievmentResponse, eMessage, isPass);

					 }


				 });

			});


		}


		/*
		Geo-fencing Hiiting the Kne Endpoint when user Away or presence
		*/
		public void updateUserPresence(bool userPresence, string groupIdentifier, BaseDelegate userDelegate)
		{
			GetUserInfo((KnetikUser user, string nerrorMessage, bool isSuccess) =>
			{

				if (!isSuccess || user.username == null)
				{
					userDelegate(null, @"Error in Getting user Info", false);
					return;
				}
				UserPresenceRequest request = new UserPresenceRequest();
				request.presence = userPresence;

				UserService.Instance.UpdateUserPresence(groupIdentifier, user.userId, request, userDelegate);

			});
		}

		public void addUserDeviceDetails(UserDevice breDelegate)
		{

		}

		String GeneratePasswordFromEmail(String email)
		{

			if (email == null || email.Length <= 0)
				return "";

			int length = email.Length;
			String password = "!PA2321985";
			if (length > 2)
				password += email.Substring(0, email.Length / 2);

			if (length > 10)
				password += email.Substring(0, 5);

			return password;

		}

	}
}

