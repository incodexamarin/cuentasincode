﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;
using JSAPISdk;
using Xamarin.Forms;
using Newtonsoft.Json.Linq;

namespace Knetik_Rest
{
	public class WeatherRepository
	{
		private static WeatherRepository instance;
		private int groupIndex;
		public static Object lockObject = new object();

		public static WeatherRepository Instance
		{

			get
			{

				if (instance == null)
				{
					lock (lockObject)
					{

						if (instance == null)
						{
							instance = new WeatherRepository();
						}
					}


				}

				return instance;
			}
		}


		private WeatherRepository()
		{
		}

		public void getWeatherByLocation(Location location,GetWeatherList weatherList)
		{
			if (location == null)
				return;
			WeatherService.Instance.getWeatherByLocation(location.latitude, location.longitude, weatherList);


		}

	}
}

