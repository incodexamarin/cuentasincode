﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;
using JSAPISdk;
using Xamarin.Forms;

namespace Knetik_Rest
{
	public class AddressRepository
	{
		private static AddressRepository instance;

		public static Object lockObject = new object();
		private string userLatitdue;
		private string userLongitude;
		private bool insideUpdateLocation;
		private int deviceId = -1;
		public static AddressRepository Instance
		{

			get
			{

				if (instance == null)
				{
					lock (lockObject)
					{

						if (instance == null)
						{
							instance = new AddressRepository();
						}
					}


				}

				return instance;
			}
		}


		private AddressRepository()
		{
		}

		public void addNewAddress(JAddressAddRequest address, SuggestAddressDelegate suggestDelegate)
		{
			UserRepository.Instance.GetUserInfo((result, errorMessage, isSuccess) => { 

				AddressService.Instance.addNewAddress(result.userId,address, suggestDelegate);
			
			});


		}

	
	}
}

