﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;

namespace Knetik_Rest
{
	public class PollRepository
	{
		private static PollRepository instance;

		public static Object lockObject = new object();

		Dictionary<String, PollResponse> pollCache;

		public static PollRepository Instance {

			get
			{

				if (instance == null)
				{
					lock(lockObject) {

						if (instance == null)
						{
							instance = new PollRepository();
							instance.pollCache = new Dictionary<string, PollResponse>();
						}
					}


				}

				return instance;
			}
		}


		private PollRepository()
		{
		}

		public void findPollByTag(String tag , PollDelegate pollDelegate) {

			if (tag == null) { 
				
				pollDelegate(null, "", false);
				return;
			}

			PollResponse response = null;
			if (pollCache.TryGetValue(tag,out response) && response != null)
			{
				
				pollDelegate(pollCache[tag], "", true);


			}else
			{
				
			PollService.Instance.GetPolls(tag, (PollResponse result, string errorMessage, bool isSuccess) => {


					if (result != null && !pollCache.ContainsKey(tag) ) {


						pollCache.Add(tag,result);

					}

				pollDelegate(result, errorMessage, isSuccess);
			
			});

			}

		}




		public void findPolls(PollDelegate pollDelegate)
		{

			PollService.Instance.GetPolls(null, pollDelegate);

		}


	}
}

