﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;
using JSAPISdk;
using Xamarin.Forms;

namespace Knetik_Rest
{
	public class QueryRepository
	{
		private static QueryRepository instance;

		public static Object lockObject = new object();
		private string userLatitdue;
		private string userLongitude;
		private bool insideUpdateLocation;
		private int deviceId = -1;
		public static QueryRepository Instance
		{

			get
			{

				if (instance == null)
				{
					lock (lockObject)
					{

						if (instance == null)
						{
							instance = new QueryRepository();
						}
					}


				}

				return instance;
			}
		}


		private QueryRepository()
		{
		}

		public void queryRequest(string filter, string unique_name, int numberofDays, QueryRequest request, QueryDelegate queryDelegate)
		{

			DateTime today = System.DateTime.UtcNow;


			long startDate = UnixTimeNow(today.AddDays(numberofDays * -1));


			long endDate = UnixTimeNow(today);

			//long startDate = 1504624553;
			//long endDate = 1507216553;


			request.query = new JSAPISdk.Query();
			request.query.category = "thermostat_data_entry";
			request.query.entity_type = "devices";
			request.query.start_date = startDate;
			request.query.end_date = endDate;
			request.query.aggregate = new JSAPISdk.Aggregate();
			request.query.aggregate.property = filter;
			request.query.aggregate.type = "avg";


			QueryService.Instance.getQuery(unique_name, request, queryDelegate);

		}

		public long UnixTimeNow(DateTime dateTime)
		{
			var timeSpan = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0));
			return (long)timeSpan.TotalSeconds;
		}


	}
}

