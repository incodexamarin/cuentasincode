﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;
using JSAPISdk;
using Xamarin.Forms;
using Newtonsoft.Json.Linq;

namespace Knetik_Rest
{
	public class GroupRepository
	{
		private static GroupRepository instance;
		private int groupIndex;
		public static Object lockObject = new object();
		private IList<UserGroup> userGroupsList;
		public static GroupRepository Instance
		{

			get
			{

				if (instance == null)
				{
					lock (lockObject)
					{

						if (instance == null)
						{
							instance = new GroupRepository();
							instance.userGroupsList = new List<UserGroup>();
						}
					}


				}

				return instance;
			}
		}


		private GroupRepository()
		{
		}

		public void getUserGroup(GetUserGroup groupDelegate)
		{

			UserRepository.Instance.GetUserInfo((KnetikUser user, string nerrorMessage, bool isSuccess) =>
			{

				if (!isSuccess || user.username == null)
				{
					groupDelegate(null, null, nerrorMessage, isSuccess);

					return;
				}

				GroupService.Instance.GetUserGroup(user.userId, groupDelegate);

			});


		}

		public void getUserGroupDetails(JArray groups, GetUserGroups groupDelegate)
		{
			int count = groups.Count;


			if (groupIndex > count)
			{

				groupDelegate(userGroupsList, null, "", true);
				return;
			}
			GroupService.Instance.GetGroupDetails(groups[groupIndex].ToString(), (groupDetails, response, errorMessage, isSuccess) =>
			{

				if (!isSuccess)
				{
					groupDelegate(userGroupsList, response, errorMessage, isSuccess);
					return;
				}

				userGroupsList.Add(groupDetails);
				groupIndex++;

				getUserGroupDetails(groups, groupDelegate);

			});


		}


		public void getUserGroups(GetUserGroups userGroups)
		{

			getUserGroup((JArray groups, BaseResponse response, string errorMessage, bool isSuccess) =>
			{

				if (!isSuccess)
				{
					userGroups(null, response, "User has no avaianle groups", isSuccess);

					return;
				}
				groupIndex = 0;
				userGroupsList.Clear();
				getUserGroupDetails(groups, userGroups);


			});

		}



	}
}

