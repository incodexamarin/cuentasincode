﻿using System;
using System.Diagnostics;
using JSAPISDK;

namespace Knetik_Rest
{
	public class BRERepository
	{
		private static BRERepository instance;
		public static Object lockObject = new object();

		public static BRERepository Instance {

			get
			{

				if (instance == null)
				{
					lock(lockObject) {

						if (instance == null)
						{
							instance = new BRERepository();
						}
					}


				}

				return instance;
			}
		}


		private BRERepository()
		{
		}

		public void fireLoginBreEvent(BREDelegate breDelegate)
		{
			UserRepository.Instance.GetUserInfo((result, errorMessage, isSuccess) => { 
			
				BREEventRequest request = new BREEventRequest();
				request.eventName = "user_login";
				request.param.Add("user", ""+result.userId);
				request.param.Add("ipAddress", "127.0.0.1");
				BREService.Instance.fireBre(request, breDelegate);
				
			});
		

		}

		public void fireWatchVideoBreEvent(BREDelegate breDelegate)
		{
			UserRepository.Instance.GetUserInfo((result, errorMessage, isSuccess) =>
			{

				BREEventRequest request = new BREEventRequest();
				request.eventName = "VIEWAVIDEO";
				request.param.Add("user", "" + result.userId);
				request.param.Add("ipAddress", "127.0.0.1");
				BREService.Instance.fireBre(request, breDelegate);

			});

		}




		public void fireFinishQuizquizBreEvent(BREDelegate breDelegate)
		{
			UserRepository.Instance.GetUserInfo((result, errorMessage, isSuccess) =>
			{

				BREEventRequest request = new BREEventRequest();
				request.eventName = "finish_first_quiz";
				request.param.Add("user", "" + result.userId);
				request.param.Add("ipAddress", "127.0.0.1");
				BREService.Instance.fireBre(request, breDelegate);

			});

		}



	}
}

