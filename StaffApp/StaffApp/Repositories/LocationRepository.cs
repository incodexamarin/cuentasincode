﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;
using JSAPISdk;
using Xamarin.Forms;
using KnetikIOT;

namespace Knetik_Rest
{
	public class LocationRepository
	{
		private static LocationRepository instance;

		public static Object lockObject = new object();
		public const int EquatorRadius = 6378137;

		public static LocationRepository Instance
		{

			get
			{

				if (instance == null)
				{
					lock (lockObject)
					{

						if (instance == null)
						{
							instance = new LocationRepository();
						}
					}


				}

				return instance;
			}
		}


		private LocationRepository()
		{
		}


		public static double DistanceBetween(Location a, Location b)
		{

			ILocation ibeacon = DependencyService.Get<ILocation>();
			return ibeacon.DistanceBetween(a.latitude, a.longitude, b.latitude, a.longitude);	


		}



		public void pushBeaconDetection(BeaconDevice beacon, BaseDelegate baseDelegate)
		{

			DeviceService.Instance.BeaconDetected(beacon, baseDelegate);

		}

	}
}

