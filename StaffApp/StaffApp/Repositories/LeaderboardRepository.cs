﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;

namespace Knetik_Rest
{
	public class LeaderboardRepository
	{
		private static LeaderboardRepository instance;

		public static Object lockObject = new object();

		Dictionary<String, ChallengeResponse> challengeResponse;

		public static LeaderboardRepository Instance {

			get
			{

				if (instance == null)
				{
					lock(lockObject) {

						if (instance == null)
						{
							instance = new LeaderboardRepository();
						}
					}


				}

				return instance;
			}
		}


		private LeaderboardRepository()
		{
		}

		public void GetLeaderboardByEventId(String eventId,LeaderboardDelegate _delegate) {

			CampaignService.Instance.GetLeaderBoardByEventId("event", eventId, _delegate);

		}


	}
}

