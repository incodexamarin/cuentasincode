﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;

namespace Knetik_Rest
{
	public class StoreRepository
	{

		private static StoreRepository instance;

		public static Object lockObject = new object();

		Dictionary<String, StoreItemResponse> challengeResponse;

		public static StoreRepository Instance
		{

			get
			{

				if (instance == null)
				{
					lock (lockObject)
					{

						if (instance == null)
						{
							instance = new StoreRepository();
							instance.challengeResponse = new Dictionary<string, StoreItemResponse>();
						}
					}


				}

				return instance;
			}
		}


		private StoreRepository()
		{
		}

		public void GetStoreItems(StoreDelegate storeDelegate)
		{

			StoreService.Instance.GetStoreItems(storeDelegate);

		}


		public void GetNewCart(String currencyCode, CartDelegate cartDelegate)
		{

			CartService.Instance.createNewCart(currencyCode, cartDelegate);

		}


		public void AddAnewCartItem(CartResponse cartObject, AddItemToCartRequest request, AddItemToCartDelegate cartDelegate)
		{

			CartService.Instance.addItemToCart(cartObject, request, cartDelegate);

		}


		public void GetCartInvoice(CartResponse cartObject, CartInvoiceRequest request, InvoiceItemDelegate cartDelegate)
		{

			CartService.Instance.GetCartInvoice(cartObject, request, cartDelegate);
		}


		public void doPaymentInvoice(InvoiceBaseResponse invoiceResponse, PaymentMethodRequest request, PaymentDelegate paymentDelegate)
		{

			CartService.Instance.doPaymentInvoice(invoiceResponse, request, paymentDelegate);
		}
	}
}

