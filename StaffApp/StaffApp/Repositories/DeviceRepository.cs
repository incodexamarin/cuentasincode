﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;
using JSAPISdk;
using Xamarin.Forms;

namespace Knetik_Rest
{
	public class DeviceRepository
	{
		private static DeviceRepository instance;

		public static Object lockObject = new object();
		private string userLatitdue;
		private string userLongitude;
		private bool insideUpdateLocation;
		private int deviceId = -1;
		public static DeviceRepository Instance
		{

			get
			{

				if (instance == null)
				{
					lock (lockObject)
					{

						if (instance == null)
						{
							instance = new DeviceRepository();
						}
					}


				}

				return instance;
			}
		}


		private DeviceRepository()
		{
		}



		public void handleUserDeviceDetails(KnetikUser user, UserDevice device, PostDeviceDelegate deviceDelagate)
		{
			return;
			UserDeviceRequest request = new UserDeviceRequest();
			request.filter_make = device.make;

			UserService.Instance.GetUserDevice(request, (UserDeviceResponse devices, string errorMessage, bool isSuccess) =>
			{

				if (!isSuccess || devices == null || devices.devices.Count <= 0)
				{

					device.deviceId = DateTime.Now.Millisecond + user.userId;
					UserService.Instance.addUserDevice(device, (postDevice, posetDeviceErrorMessage, post_isSuccess) =>
					{

						deviceDelagate(postDevice, posetDeviceErrorMessage, post_isSuccess);
						return;

					});

				}
				else
				{
					UserDevice updatedDevice = devices.devices[0];
					updatedDevice.data = new Data();
					updatedDevice.data.push_notification_token = device.data.push_notification_token;
					updatedDevice.data.token = device.data.token;


					UserService.Instance.UpdateUserDevice(updatedDevice, (UDevice, UErrorMessage, UisSuccess) =>
					{

						deviceDelagate(UDevice, UErrorMessage, UisSuccess);

					});
				}



			});


		}


		public void getBeaconDevices(GetDeviceDelagates devices)
		{
			UserDeviceResponse userDevice = new UserDeviceResponse();
			userDevice.devices = new List<UserDevice>();

			UserDevice device1 = new UserDevice();
			device1.uuid = "EBEFD083-70A2-47C8-9837-E7B5634DF524";
			device1.major = 47197;
			device1.minor = 52877;
			device1.url = "https://iot.portal.devsandbox.knetikcloud.com";

			UserDevice device2 = new UserDevice();
			device2.uuid = "EBEFD083-70A2-47C8-9837-E7B5634DF524";
			device2.major = 1;
			device2.minor = 1;
			device2.url = "http://iot.knetik.com";
			device2.url = "https://iot.portal.devsandbox.knetikcloud.com";


			UserDevice device3 = new UserDevice();
			device3.uuid = "EBEFD083-70A2-47C8-9837-E7B5634DF524";
			device3.major = 52927;
			device3.minor = 3779;


			UserDevice device4 = new UserDevice();
			device4.uuid = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
			device4.major = 45591;
			device4.minor = 65375;


			UserDevice device5 = new UserDevice();
			device5.uuid = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
			device5.major = 151;
			device5.minor = 60271;


			UserDevice device6 = new UserDevice();
			device6.uuid = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
			device6.major = 150;
			device6.minor = 48855;


			UserDevice device7 = new UserDevice();
			device7.uuid = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
			device7.major = 152;
			device7.minor = 26106;

			//userDevice.devices.Add(device7);
			//userDevice.devices.Add(device6);
			//userDevice.devices.Add(device5);
			//userDevice.devices.Add(device4);
			//userDevice.devices.Add(device3);
			//userDevice.devices.Add(device2);
			userDevice.devices.Add(device1);

			devices(userDevice, "", true);

		}


		public void pushBeaconDetection(BeaconDevice beacon, BaseDelegate baseDelegate)
		{

			DeviceService.Instance.BeaconDetected(beacon, baseDelegate);

			pushBeaconIOTDetection(beacon, (Newtonsoft.Json.Linq.JObject response, string errorMessage, bool isSuccess) => {

				Console.WriteLine("Request sent to with response  " + isSuccess);
			
			});

		}

		public void pushBeaconIOTDetection(BeaconDevice beacon, GeneralDelagate baseDelegate)
		{
			BeaconIOTRequest iotRequest = new BeaconIOTRequest();
			iotRequest.device_mac = beacon.Id1;
			iotRequest.beacon = beacon;
			string endpoint = "https://j0s4zw7ed6.execute-api.us-east-1.amazonaws.com/dev/thermostats/data/raw";
				CustomService.Instance.postToCustomEndpoint(endpoint, JSAPIHandler.Instance.GetTokenType, JSAPIHandler.Instance.GetToken, iotRequest,baseDelegate);
		}

	}
}

