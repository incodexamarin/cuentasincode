﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;

namespace Knetik_Rest
{
	public class CampaignRepository
	{
		private static CampaignRepository instance;

		public static Object lockObject = new object();

		Dictionary<String, CampaignsResponse> campaignResponse;

		public static CampaignRepository Instance {

			get
			{

				if (instance == null)
				{
					lock(lockObject) {

						if (instance == null)
						{
							instance = new CampaignRepository();
							instance.campaignResponse = new Dictionary<string, CampaignsResponse>();
						}
					}


				}

				return instance;
			}
		}


		private CampaignRepository()
		{
		}

		public void GetCampaign(CampaignDelegate campaignDelegate) {

			CampaignService.Instance.GetCampaigns(campaignDelegate);

		}

	


	}
}

