﻿using System;
using System.Diagnostics;
using JSAPISDK;
using System.Collections.Generic;
using JSAPISdk;
using Xamarin.Forms;
using Newtonsoft.Json.Linq;

namespace Knetik_Rest
{
	public class ThermostateRepository
	{
		private static ThermostateRepository instance;
		public static Object lockObject = new object();

		public static ThermostateRepository Instance
		{

			get
			{

				if (instance == null)
				{
					lock (lockObject)
					{

						if (instance == null)
						{
							instance = new ThermostateRepository();
						}
					}


				}

				return instance;
			}
		}


		private ThermostateRepository()
		{
		}

		public void getThermostateTemperature(ThermostateRequest request, GetThermostateDetailsList thermostateList)
		{
			ThermostatService.Instance.getThermostatDetails(request, thermostateList);
		}

		public void UpdateThermostateTemperature(ThermostateUpdateRequest request, GetThermostateDetailsList thermostateList)
		{
			ThermostatService.Instance.updateThermostatDetails(request, thermostateList);
		}

		public void UpdateThermostateHold(ThermostateUpdateHoldRequest request, GetThermostateDetailsList thermostateList)
		{
			ThermostatService.Instance.updateThermostatHold(request, thermostateList);
		}

		public void getUserThermostate(UserThermostatRequest request,string groupId, ThermostatUsersDelegate thermostateUserDelegate)
		{
			ThermostatService.Instance.getThermostatMembers(request,groupId ,thermostateUserDelegate);
		}

	}
}

