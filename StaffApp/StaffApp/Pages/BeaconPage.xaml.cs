﻿using System;
using System.Collections.Generic;
using Knetik_Rest;
using Xamarin.Forms;
using JSAPISDK;
using BluetoothLE.Core;
using BluetoothLE.Core.Events;
using System.Collections.ObjectModel;
using JSAPISdk;
using KnetikIOT;
using Plugin.Media;
using Plugin.Media.Abstractions;

namespace StaffApp
{
	public partial class BeaconPage : ContentPage, IBeaconStatusChanged
	{

		private bool insideReload;
		private IAdapter _bluetoothAdapter;
		public IAdapter BluetoothAdapter { get { return _bluetoothAdapter; } }
		public ObservableCollection<BeaconDevice> DiscoveredDevices { get; private set; }
		public Dictionary<string, IProximity> proximtryChange;
		List<BeaconDevice> beaconDevices;

		public BeaconPage()
		{

			InitializeComponent();
			_bluetoothAdapter = DependencyService.Get<IAdapter>();
			proximtryChange = new Dictionary<string, IProximity>();

			DiscoveredDevices = new ObservableCollection<BeaconDevice>();

			deviceListView.ItemsSource = DiscoveredDevices;
			deviceListView.ItemSelected += DeviceSelected;


			IBeacon ibeacon = DependencyService.Get<IBeacon>();
			ibeacon.VerityBluetooth();
			ibeacon.RegisterBeaconDelegate(this);


		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			var file = CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
			{
				DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Front
			});


		}

		void DeviceSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var device = e.SelectedItem as IDevice;
			if (device != null)
			{
				BluetoothAdapter.ConnectToDevice(device);
			}
		}

		#region BluetoothAdapter callbacks

		void DeviceDiscovered(object sender, DeviceDiscoveredEventArgs e)
		{
			//DiscoveredDevices.Add(e.Device);
			Device.BeginInvokeOnMainThread(() =>
			{
				deviceListView.ItemsSource = DiscoveredDevices;
				deviceListView.BeginRefresh();
				deviceListView.EndRefresh();
			});

		}

		void DeviceConnected(object sender, DeviceConnectionEventArgs e)
		{
			// Navigation.PushAsync(new DevicePage(e.Device));
		}


		#endregion

		async void onLogoutClicked(object sender, EventArgs e)
		{
			var answer = await DisplayAlert("", "Are you sure you want to logout?", "Logout", "cancel");

			if (Equals(answer, true))
			{
				await Navigation.PopAsync(true);

			}

		}

		async void onHomeClicked(object sender, EventArgs e)
		{
			//ToolbarItem tbi = (ToolbarItem)sender;

			await Navigation.PopAsync(true);

		}

		public void becaonStatus(UserDevice device, IProximity proximity)
		{


		}

		public void updateBeaconsList(IList<BeaconDevice> _devices)
		{

			if (insideReload)
				return;

			beaconDevices = new List<BeaconDevice>();
			beaconDevices.Clear();
			beaconDevices.AddRange(_devices);
			insideReload = true;
			DiscoveredDevices.Clear();

			if (beaconDevices.Count > 0)
			{
				foreach (BeaconDevice d in beaconDevices)
				{

					DiscoveredDevices.Add(d);

					IProximity cu_proximity = Util.GetProximityFromDistance(d.Distance);
					string Key = d.Id1 + d.major;
					IProximity proximity = proximtryChange.GetValueOrDefault(Key, IProximity.LOST);
					if (proximity != cu_proximity)
					{
						proximtryChange.TryAdd(Key, cu_proximity);
						showProximityNotification(d, cu_proximity);

						DeviceRepository.Instance.pushBeaconDetection(d, (result, errorMessage, isSuccess) =>
						{

							Console.WriteLine("Beacon detected");
						});

					}

					Device.BeginInvokeOnMainThread(() =>
					{
						camera_lbl.Text = d.Distance + "     " + d.Rssi;
					});
				}
			}

			Device.BeginInvokeOnMainThread(() =>
			{
				deviceListView.BeginRefresh();
				deviceListView.ItemsSource = DiscoveredDevices;

				deviceListView.EndRefresh();
				insideReload = false;
			});

		}

		void showProximityNotification(BeaconDevice device, IProximity proximity)
		{
			string notification = " The Beacon with major = " + device.major + " & Minor = " + device.minor + " has - Proximity : ";



			switch (proximity)
			{

				case IProximity.NEAR:
					{
						notification += "Near";
					}
					break;
				case IProximity.MEDUIM:
					{
						notification += "Meduim";
					}
					break;
				case IProximity.FAR:
					{
						notification += "Far";
					}
					break;

			}
			if (device.url != null && device.url.Length > 0)
			{

				notification += " \n With Url " + device.url;
			}

			Device.BeginInvokeOnMainThread(() =>
		{
			IToastNotification iToast = DependencyService.Get<IToastNotification>();
			iToast.showToast(notification);
		});


		}


		public void onEnterRegion(BeaconDevice beaconDevice)
		{

		}

		public void onExitRegion(BeaconDevice beaconDevice)
		{


		}

		public void displayAlertMessage(string message)
		{
			DisplayAlert("Info", message, "Ok");
		}

		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			Console.WriteLine("Beacon detected");
			BeaconDevice beaconDevice = (BeaconDevice)e.Item;
			if (beaconDevice.url != null && beaconDevice.url.Length > 0)
			{

				Device.OpenUri(new Uri(beaconDevice.url));

			}
		}

		public void devicePositionChanged(double x, double y, double z)
		{
			//camera_lbl.Text = "Camera X : " + x + " Y :" + y + "   Z:" + z;
		}
	}
}
