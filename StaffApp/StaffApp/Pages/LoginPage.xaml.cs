﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using JSAPISdk;
using JSAPISDK;
using Knetik_Rest;
using KnetikIOT;
using Xamarin.Forms;

namespace StaffApp
{
	public partial class LoginPage : ContentPage, FacebookLoginCompleted
	{
		string email = "";
		string password = "";

		FacebookRequestnterface requesedFacebook;
		public bool IsLoading { get; set; }




		void Handle_Clicked(object sender, System.EventArgs e)
		{
			Navigation.PushAsync(new SignUpPage());
		}

		void Handle_FacebookLogin(object sender, System.EventArgs e)
		{
			requesedFacebook.requestFacebookLogin(this);

		}

		public LoginPage(FacebookRequestnterface rFacebook)
		{
			this.requesedFacebook = rFacebook;
			InitializeComponent();

		}



		void Handle_Completed(object sender, System.EventArgs e)
		{

		}



		protected override void OnAppearing()
		{
			base.OnAppearing();


		}

		void Password_Text_Completed(object sender, System.EventArgs e)
		{
			this.password = ((Entry)sender).Text;
		}

		void Email_Text_Completed(object sender, System.EventArgs e)
		{
			this.email = ((Entry)sender).Text;

		}



		void Handle_Login(object sender, System.EventArgs e)
		{
			
			if (!Regex.Match(email.ToString(), @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
			{

				showAlert("Error", "Incorrect Email Addess");
				return;
			}


			if (password.Length < 5)
			{
				showAlert("Error", "Password should be at least 5 characters");
				return;

			}


			KnetikUser user = new KnetikUser();

			user.email = email;
			user.username = email;
			user.password = password;
			showLoadingAlert();
			IsLoading = true;
			UserRepository.Instance.LoginWithUserNameAndPassword(email, password, (result, errorMessage, isSuccess) =>
			{
				hideLoadingAlert();
				IsLoading = false;
				if (isSuccess)
				{

					Navigation.PushAsync(new HomePage());

				}
				else
				{

					showAlert("Error", "Incorrect User Name or Password. Please try again.");

				}


			});
		}

		public void showAlert(string alert, string message)
		{
			DisplayAlert(alert, message, "Ok");

		}

		public void facebookCompleted(string token)
		{
			showLoadingAlert();

			UserRepository.Instance.LoginWithFacebookAccount(token, (result, errorMessage, isSuccess) =>
		{
			Console.WriteLine(errorMessage);
			Console.WriteLine(result.ToString());
			hideLoadingAlert();
			if (isSuccess)
			{

				Navigation.PushAsync(new HomePage());

			}
			else
			{

				showAlert("Error", result.error_description);

			}


		});
			System.Console.WriteLine(token);
		}


		void showLoadingAlert()
		{
			this.Content.IsEnabled = false;


			//	this.activityIndicator.IsRunning = true;

		}

		void hideLoadingAlert()
		{
			this.Content.IsEnabled = true;

			//	this.activityIndicator.IsRunning = false;
		}

		public void becaonStatus(UserDevice device, IProximity proximity)
		{
		}

		public void updateBeaconsList(IList<UserDevice> devices)
		{
		}

		public void facebookError(string error)
		{
			DisplayAlert("Error", error, "Ok");

		}
	}
}