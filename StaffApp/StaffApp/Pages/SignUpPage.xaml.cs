﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using JSAPISDK;
using Knetik_Rest;
using Xamarin.Forms;

namespace StaffApp
{
	public partial class SignUpPage : ContentPage
	{
		void Handle_Completed(object sender, System.EventArgs e)
		{
			throw new NotImplementedException();
		}

		string firstName = "";
		string lastName = "";
		string email = "";
		string emailConfirmation = "";
		string password = "";
		string passwordConfirmation = "";

		void FirstName_Text_Completed(object sender, System.EventArgs e)
		{
			this.firstName = ((Entry)sender).Text;
		}

		void LastName_Text_Completed(object sender, System.EventArgs e)
		{
			this.lastName = ((Entry)sender).Text;
		}



		void Password_Text_Completed(object sender, System.EventArgs e)
		{
			this.password = ((Entry)sender).Text;
		}

		void Password_Confirmation_Text_Completed(object sender, System.EventArgs e)
		{
			this.passwordConfirmation = ((Entry)sender).Text;
		}

		void Email_Text_Completed(object sender, System.EventArgs e)
		{
			this.email = ((Entry)sender).Text;

		}

		void Email_Confirmation_Text_Completed(object sender, System.EventArgs e)
		{
			this.emailConfirmation = ((Entry)sender).Text;

		}


		void Handle_Cancel(object sender, System.EventArgs e)
		{
			Navigation.PopAsync(true);
		}

		void Handle_Submit(object sender, System.EventArgs e)
		{
			if (firstName.Length <= 0)
			{
				showAlert("Error", "Please Fill Required Field, First Name");
				return;

			}
			if (lastName.Length <= 0)
			{
				showAlert("Error", "Please Fill Required Field, Last Name");
				return;

			}

			if (email != emailConfirmation || !Regex.Match(email.ToString(), @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
			{

				showAlert("Error", "Incorrect Email Addess");
				return;
			}


			if (password.Length < 5)
			{
				showAlert("Error", "Password should be at least 5 characters");
				return;

			}

			if (!password.Equals(passwordConfirmation))
			{


				showAlert("Error", "Password And Confirm Password should be the same");
				return;
			}


			KnetikUser user = new KnetikUser();

			user.email = email;
			user.username = email;
			user.password = password;
			user.first_name = firstName;
			user.last_name = lastName;
			user.display_name = firstName;
			user.additional_properties = new UserAdditionalProperties();
			user.additional_properties.latitude = new Latitude();
			user.additional_properties.latitude.value = "0.0";
			user.additional_properties.latitude.type = "text";
			user.additional_properties.longitude = new Longitude();
			user.additional_properties.longitude.value = "0.0";
			user.additional_properties.longitude.type = "text";
			UserRepository.Instance.registerANewUser(user, (result, errorMessage, isSuccess) =>
			{


				if (isSuccess)
				{
					showAlert("Success", "Registeration has been completed please LoginIn");
					Navigation.PopAsync(true);
				}
				else
				{

					showAlert("Error", errorMessage);
				}





			});

		}

		public void showAlert(string alert, string message)
		{
			DisplayAlert(alert, message, "Ok");

		}
		public SignUpPage()
		{
			InitializeComponent();
		}
	}
}
