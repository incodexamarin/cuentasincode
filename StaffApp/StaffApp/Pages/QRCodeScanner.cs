﻿using System;
using JSAPISDK;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace StaffApp
{
	public class QRCodeScanner : ContentPage
	{
		ZXingScannerView zxing;
		ZXingDefaultOverlay overlay;

		public QRCodeScanner() : base()
		{
			zxing = new ZXingScannerView
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand
			};
			zxing.OnScanResult += (result) =>
			{
				// Stop analysis until we navigate away so we don't keep reading barcodes
				zxing.IsAnalyzing = false;
				zxing.IsScanning = false;
				Device.BeginInvokeOnMainThread(async () =>
				{

					KnetikUser user = new KnetikUser();
					user.userId = Int32.Parse(result.Text);
					await Navigation.PushAsync(new UserDetailsPage(user));



				});
			};
			overlay = new ZXingDefaultOverlay
			{
				TopText = "Knetik QR Code",
				BottomText = "Scanning QRCodes",
				ShowFlashButton = zxing.HasTorch,
			};
			overlay.FlashButtonClicked += (sender, e) =>
			{
				zxing.IsTorchOn = !zxing.IsTorchOn;
			};
			var grid = new Grid
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};
			grid.Children.Add(zxing);
			grid.Children.Add(overlay);

			// The root page of your application
			Content = grid;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			zxing.IsScanning = true;
		}

		protected override void OnDisappearing()
		{
			zxing.IsScanning = false;

			base.OnDisappearing();
		}
	}
}

