﻿using System;
using System.Collections.Generic;
using JSAPISDK;
using Knetik_Rest;
using Xamarin.Forms;
using System.Text.RegularExpressions;

namespace StaffApp
{
	public partial class UserDetailsPage : ContentPage
	{
		string firstName = "";
		string lastName = "";
		string email = "";
		string notes = "";
		string stamps = "";
		KnetikUser knetikUser = null;
		public UserDetailsPage(KnetikUser user)
		{
			InitializeComponent();
			this.knetikUser = user;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			if (knetikUser != null) {

				UserRepository.Instance.GetUserInfoById(""+knetikUser.userId, (KnetikUser result, string errorMessage, bool isSuccess) => {

					Device.BeginInvokeOnMainThread(() => {

						user_id_entry.Text = ""+result.userId;
						first_name_entry.Text = result.first_name;
						email_entry.Text = result.email;
						last_name_entry.Text = result.last_name;

						if (result.additional_properties != null && result.additional_properties.notes != null && result.additional_properties.notes.value != null)
						{
							notes_entry.Text = result.additional_properties.notes.value;
						}
						
					});

				});
			
			}

		}

		void Handle_Cancel(object sender, System.EventArgs e)
		{
			Navigation.PopAsync(true);
		}

		void Handle_Submit(object sender, System.EventArgs e)
		{
			firstName = first_name_entry.Text;
			lastName = last_name_entry.Text;
			email = email_entry.Text;

			if (firstName == null || firstName.Length <= 0)
			{
				showAlert("Error", "Please Fill Required Field, First Name");
				return;

			}
			if (lastName == null || lastName.Length <= 0)
			{
				showAlert("Error", "Please Fill Required Field, Last Name");
				return;

			}

			if (email == null || email.Length <= 0 || !Regex.Match(email.ToString(), @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
			{

				showAlert("Error", "Incorrect Email Addess");
				return;
			}



			KnetikUser user = new KnetikUser();
			user.userId = knetikUser.userId;
			user.email = email;
			user.username = email;
			user.first_name = firstName;
			user.last_name = lastName;
			user.display_name = firstName;
			user.additional_properties = new UserAdditionalProperties();
			user.additional_properties.latitude = new Latitude();
			user.additional_properties.latitude.value = "0.0";
			user.additional_properties.latitude.type = "text";
			user.additional_properties.longitude = new Longitude();
			user.additional_properties.longitude.value = "0.0";
			user.additional_properties.longitude.type = "text";

			user.additional_properties.notes = new Notes();
			user.additional_properties.notes.type = "text";
			user.additional_properties.notes.value = notes_entry.Text;
			user.tags = new List<object>();
			UserRepository.Instance.UpdateUserInfo(user, (result, errorMessage, isSuccess) =>
			{


				if (isSuccess)
				{
					showAlert("Success", "Registeration has been completed please LoginIn");
					Navigation.PopAsync(true);
				}
				else
				{

					showAlert("Error", errorMessage);
				}





			});
		}

		public void showAlert(string alert, string message)
		{
			DisplayAlert(alert, message, "Ok");
		}
	}
}
