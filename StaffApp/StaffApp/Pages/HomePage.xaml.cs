﻿using System;
using System.Collections.Generic;
using Knetik_Rest;
using Xamarin.Forms;
using JSAPISDK;
using System.Collections.ObjectModel;
using JSAPISdk;
using KnetikIOT;

namespace StaffApp
{
	public partial class HomePage : ContentPage
	{
		void QR_Scanner(object sender, System.EventArgs e)
		{
			Navigation.PushAsync(new QRCodeScanner());
		}

		public HomePage()
		{

			InitializeComponent();

			UserRepository.Instance.updateUserToken((result, errorMessage, isSuccess) =>
			{
				Console.WriteLine(isSuccess);
			});

		}

		async void onLogoutClicked(object sender, EventArgs e)
		{
			var answer = await DisplayAlert("", "Are you sure you want to logout?", "Logout", "cancel");

			if (Equals(answer, true))
			{
				await Navigation.PopAsync(true);

			}

		}

		void onHomeClicked(object sender, EventArgs e)
		{


		}

		public void displayAlertMessage(string message)
		{
			DisplayAlert("Info", message, "Ok");
		}
	}
}
