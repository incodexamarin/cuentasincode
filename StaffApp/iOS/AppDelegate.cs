﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using JSAPISDK;
using KnetikIOT.iOS;
using UIKit;
using Xamarin.Forms;

namespace StaffApp.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();
		
			JSAPIHandler.init(Knetik_Rest.Constants.KNETIK_JSPAI_URL, "mobilClient", "", Knetik_Rest.Constants.KNETIK_NOTIFICATION_URL);

			ZXing.Net.Mobile.Forms.iOS.Platform.Init();
			DependencyService.Register<ToastManger>();
			DependencyService.Register<BecaonsManger>();


			LoadApplication(new App());


			return base.FinishedLaunching(app, options);
		}
	}
}
