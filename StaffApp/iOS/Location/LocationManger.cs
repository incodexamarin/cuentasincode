﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreLocation;
using Foundation;
using JSAPISdk;
using Knetik_Rest;
using KnetikIOT.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(LocationManger))]
namespace KnetikIOT.iOS
{
	public class LocationManger : ILocation
	{

		CLLocationManager locMgr;

		ILocationHasChanged locationManger;

		public LocationManger()
		{
			this.locMgr = new CLLocationManager();
			this.locMgr.RequestAlwaysAuthorization(); // works in background
			this.locMgr.AllowsBackgroundLocationUpdates = true;
			this.locMgr.PausesLocationUpdatesAutomatically = false; // works in background
			this.locMgr.StartUpdatingLocation();
			this.locMgr.DistanceFilter = 1;

			this.locMgr.LocationsUpdated += (object sender, CLLocationsUpdatedEventArgs e) =>
			{

				if (e.Locations.Length <= 0)
					return;

				CLLocation location = e.Locations[0];

				if (locationManger != null)
				{

					Location Userlocation = new Location();
					Userlocation.latitude = location.Coordinate.Latitude;
					Userlocation.longitude = location.Coordinate.Longitude;
					this.locationManger.locationChanged(Userlocation);
				}


			};

		}

		public void registerLocationDelegate(ILocationHasChanged locationM)
		{
			this.locationManger = locationM;

		}

		public void verifyLocation()
		{
			locMgr.AuthorizationChanged += (sender, args) =>
				{
					if (args.Status == CLAuthorizationStatus.Denied)
					{

						if (this.locationManger != null)
						{

							this.locationManger.displayAlertMessage("Iot not able to access your location please check it. Go to Setting -> privacy");

						}
					}
					Console.WriteLine("Authorization changed to: {0}", args.Status);
				};

		}

		public void onLocationResume()
		{
			
		}

		public void RemoveUpdates()
		{

		}

		public void OnConnected()
		{

		}

		public double DistanceBetween(double startLatitude, double startLongitude, double endLatitude, double endLongitude)
		{
			CLLocation pointA = new CLLocation(startLatitude,startLongitude);
			CLLocation pointB = new CLLocation(endLatitude, endLongitude);
			var distanceToB = pointB.DistanceFrom(pointA);
			return distanceToB;
		}

		public Location getCurrentLocation()
		{
			return null;
		}
	}
}
