﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreLocation;
using CoreMotion;
using EddyStoneScanner;
using Foundation;
using JSAPISdk;
using Knetik_Rest;
using KnetikIOT.iOS;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(BecaonsManger))]
namespace KnetikIOT.iOS
{
	public class BecaonsManger : ESSBeaconScannerDelegate, IBeacon
	{

		NSMutableArray beaconRegions;
		CLLocationManager locationMgr;
		private readonly List<BeaconDevice> beaconsList;
		private static List<BeaconDevice> eddyStoneList = new List<BeaconDevice>();
		private static List<String> eddystoneIds = new List<String>();

		private Dictionary<string, string> urlContainer;
		IBeaconStatusChanged bluManger;
		CMMotionManager motionManager;


		public BecaonsManger()
		{


			motionManager = new CMMotionManager();
			motionManager.AccelerometerUpdateInterval = 1.0;
			beaconsList = new List<BeaconDevice>();
			urlContainer = new Dictionary<string, string>();
			beaconRegions = new NSMutableArray();
			locationMgr = new CLLocationManager();
			locationMgr.RequestWhenInUseAuthorization();


			locationMgr.RegionEntered += RegionEntered;
			locationMgr.DidRangeBeacons += DidRangeBeacons;

			DeviceRepository.Instance.getBeaconDevices((devices, errorMessage, isSuccess) =>
			{

				if (!isSuccess)
					return;


				foreach (UserDevice device in devices.devices)
				{

					var udid = new NSUuid(device.uuid);
					CLBeaconRegion beaconRegion = new CLBeaconRegion(udid, "Major:" + device.major + "& Minor:" + device.minor);

					beaconRegion.NotifyOnEntry = true;
					beaconRegion.NotifyOnExit = true;
					beaconRegion.NotifyEntryStateOnDisplay = true;
					locationMgr.StartMonitoring(beaconRegion);
					locationMgr.StartRangingBeacons(beaconRegion);


					beaconRegions.Add(beaconRegion);

					if (device.url != null && device.url.Length > 0 && urlContainer != null)
					{
						urlContainer.Add(device.uuid, device.url);
					}
				}
			});


			ESSBeaconScanner ess = new ESSBeaconScanner();
			ess.Delegate = new eddyStoneMangerB();
			ess.StartScanning();

			motionManager.StartAccelerometerUpdates(NSOperationQueue.CurrentQueue, (data, error) =>
			{
				bluManger.devicePositionChanged(data.Acceleration.X,data.Acceleration.Y,data.Acceleration.Z);
			});

		}




		class eddyStoneMangerB : ESSBeaconScannerDelegate
		{
			public override void DidFindBeacon(ESSBeaconScanner scanner, NSObject beaconInfo)
			{

				Console.WriteLine(beaconInfo);

			}
			public override void DidLoseBeacon(ESSBeaconScanner scanner, NSObject beaconInfo)
			{

				Console.WriteLine(beaconInfo);

			}
			public override void DidUpdateBeacon(ESSBeaconScanner scanner, NSObject beaconInfo)
			{

				Console.WriteLine(beaconInfo);

			}
			public override void DidFindURL(ESSBeaconScanner scanner, NSUrl url)
			{
				Console.WriteLine(url.ToString());

				if (!eddystoneIds.Contains(url.ToString()))
				{
					eddystoneIds.Add(url.ToString());
					BeaconDevice device = new BeaconDevice();
					device.url = url.ToString();
					device.Id1 = "EddyStone1";
					eddyStoneList.Add(device);
				}

			}
		};



		void RegionEntered(object sender, CLRegionEventArgs e)
		{

			Console.WriteLine("Did enter region");

		}

		async void DidRangeBeacons(object sender, CLRegionBeaconsRangedEventArgs e)
		{

			beaconsList.Clear();
			var allBeacons = new List<CLBeacon>();
			bool containsLocation = e.Region.Contains(new CLLocationCoordinate2D());

			if (e.Beacons.Length > 0)
			{

				if (e.Beacons != null && e.Beacons.Length > 0)
				{
					foreach (var b in e.Beacons)
					{

						allBeacons.Add(b);
					}

					var orderedBeacons = allBeacons.OrderBy(b => b.Accuracy).ToList();
					allBeacons = orderedBeacons;
				}


			}

			await UpdateData(allBeacons);


		}

		async Task UpdateData(List<CLBeacon> beacons)
		{
			await Task.Run(() =>
			{
				var newBeacons = new List<CLBeacon>();

				foreach (var beacon in beacons)
				{
					if (beaconsList.Exists(b => b.major.ToString() == beacon.Major.ToString()))
					{
						//update data
						var index = beaconsList.FindIndex(b => b.Id1.ToString() == beacon.Major.ToString());
						if (index != -1)
						{
							beaconsList[index] = getDeviceBeacon(beacon);
						}
					}
					else
					{
						newBeacons.Add(beacon);
					}
				}


				foreach (var beacon in newBeacons)
				{
					beaconsList.Add(getDeviceBeacon(beacon));
				}

				if (newBeacons.Count > 0)
				{
					beaconsList.Sort((x, y) => x.Distance.CompareTo(y.Distance));
				}

				if (bluManger != null)
				{
					beaconsList.AddRange(eddyStoneList);
					bluManger.updateBeaconsList(beaconsList);
				}


			});

		}


		BeaconDevice getDeviceBeacon(CLBeacon beacon)
		{
			BeaconDevice device = new BeaconDevice();
			device.Distance = beacon.Accuracy;
			device.Id1 = beacon.ProximityUuid.ToString();
			device.major = beacon.Major.ToString();
			device.minor = beacon.Minor.ToString();
			device.Rssi = (int)beacon.Rssi;
			device.TxPower = (int)beacon.Rssi;
			device.url = urlContainer.GetValueOrDefault(beacon.ProximityUuid.ToString(), "");

			Console.WriteLine("Distance Length     :" + device.Distance);
			return device;
		}


		public void NotifyServiceConnection()
		{

		}

		public void OnBackGroundSearch()
		{


		}

		public void OnClose()
		{

		}

		public void OnPauseSearch()
		{

		}

		public void OnResumeSearch()
		{

		}

		public void OnstopSearch()
		{

		}

		public void RegisterBeaconDelegate(IBeaconStatusChanged beaconStatus)
		{
			this.bluManger = beaconStatus;
		}

		public void VerityBluetooth()
		{
			var manager = new CLLocationManager();
			manager.AuthorizationChanged += (sender, args) =>
			{
				if (args.Status == CLAuthorizationStatus.Denied)
				{

					if (bluManger != null)
					{

						bluManger.displayAlertMessage("Iot not able to access your location please check it. Go to Setting -> privacy");

					}
				}
				Console.WriteLine("Authorization changed to: {0}", args.Status);
			};

		}
	}
}
