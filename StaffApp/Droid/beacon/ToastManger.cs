﻿using System;
using Android.Widget;
using KnetikIOT.Droid;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(ToastManger))]
namespace KnetikIOT.Droid
{
	public class ToastManger : Java.Lang.Object, IToastNotification
	{
		public ToastManger()
		{
		}

		public void showToast(string message)
		{
			Toast.MakeText(Forms.Context, message, ToastLength.Short).Show();

		}
	}
}
