﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AltBeaconOrg.BoundBeacon;
using AltBeaconOrg.BoundBeacon.Utils;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using JSAPISdk;
using KnetikIOT.Droid;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(BecaonsManger))]
namespace KnetikIOT.Droid
{
	public class BecaonsManger : Java.Lang.Object, IBeacon
	{
		IBeaconStatusChanged bluManger;
		private readonly RangeNotifier _rangeNotifier;
		private readonly MonitorNotifier _monitorNotifier;

		AltBeaconOrg.BoundBeacon.Region _tagRegion, _tagRegion2, _instanceRegion, _myeddystone, _realeddystone2;

		private BeaconManager _beaconManager;
		private readonly List<BeaconDevice> _data;

		public Context ApplicationContext
		{
			get
			{
				return Forms.Context;
			}
		}



		public BecaonsManger()
		{
			_rangeNotifier = new RangeNotifier();
			_monitorNotifier = new MonitorNotifier();
			_data = new List<BeaconDevice>();
			_beaconManager = BeaconManager.GetInstanceForApplication(Forms.Context);

			var iBeaconParser = new BeaconParser();
			//	Estimote > 2013
			iBeaconParser.SetBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24");

			var ieddyParser = new BeaconParser();
			//	Estimote > 2013
			ieddyParser.SetBeaconLayout(BeaconParser.EddystoneUidLayout);


			var ieddyParser2 = new BeaconParser();
			ieddyParser2.SetBeaconLayout(BeaconParser.EddystoneUrlLayout);


			var ieddyParser3 = new BeaconParser();
			ieddyParser3.SetBeaconLayout(BeaconParser.EddystoneTlmLayout);


			_beaconManager.BeaconParsers.Add(iBeaconParser);
			_beaconManager.BeaconParsers.Add(ieddyParser);
			_beaconManager.BeaconParsers.Add(ieddyParser2);
			_beaconManager.BeaconParsers.Add(ieddyParser3);

			_beaconManager.Bind((IBeaconConsumer)Xamarin.Forms.Forms.Context);
			_beaconManager.SetBackgroundMode(false);

			_monitorNotifier.EnterRegionComplete += EnteredRegion;
			_monitorNotifier.ExitRegionComplete += ExitedRegion;
			_monitorNotifier.DetermineStateForRegionComplete += DeterminedStateForRegionComplete;

			_rangeNotifier.DidRangeBeaconsInRegionComplete += RangingBeaconsInRegion;
		}



		public void RegisterBeaconDelegate(IBeaconStatusChanged beaconStatus)
		{
			this.bluManger = beaconStatus;
		}

		public void VerityBluetooth()
		{
			try
			{
				if (!BeaconManager.GetInstanceForApplication(Forms.Context).CheckAvailability())
				{
					var builder = new AlertDialog.Builder(Forms.Context);
					builder.SetTitle("Bluetooth not enabled");
					builder.SetMessage("Please enable bluetooth in settings and restart this application.");
					EventHandler<DialogClickEventArgs> handler = null;
					builder.SetPositiveButton(Android.Resource.String.Ok, handler);
					//					builder.SetOnDismissListener(this.context);
					builder.Show();
				}
			}
			catch (BleNotAvailableException e)
			{
				Log.Debug("BleNotAvailableException", e.Message);

				var builder = new AlertDialog.Builder(Forms.Context);
				builder.SetTitle("Bluetooth LE not available");
				builder.SetMessage("Sorry, this device does not support Bluetooth LE.");
				EventHandler<DialogClickEventArgs> handler = null;
				builder.SetPositiveButton(Android.Resource.String.Ok, handler);
				builder.Show();
			}


		}

		async void RangingBeaconsInRegion(object sender, RangeEventArgs e)
		{
			var allBeacons = new List<Beacon>();
			if (e.Beacons != null && e.Beacons.Count > 0)
			{
				foreach (var b in e.Beacons)
				{
					allBeacons.Add(b);
				}

				var orderedBeacons = allBeacons.OrderBy(b => b.Distance).ToList();
				await UpdateData(orderedBeacons);
			}
			else
			{
				await UpdateData(allBeacons);
			}
		}

		void RemoveBeaconsNoLongerVisible(List<Beacon> allBeacons)
		{
			if (allBeacons == null || allBeacons.Count == 0) return;

			var deleteBeacons = new List<BeaconDevice>();

			foreach (var d in _data)
			{
				if (allBeacons.All(ab => ab.Id1.ToString() != d.Id1.ToString()))
				{
					deleteBeacons.Add(d);
				}
			}

			_data.RemoveAll(d => deleteBeacons.Any(del => del.Id1.ToString() == d.Id1.ToString()));

			if (deleteBeacons.Count > 0)
			{
				deleteBeacons = null;
			}
		}
		async Task UpdateData(List<Beacon> beacons)
		{
			await Task.Run(() =>
			{
				var newBeacons = new List<Beacon>();

				foreach (var beacon in beacons)
				{
					if (_data.Exists(b => b.Id1.ToString() == beacon.Id1.ToString()))
					{
						//update data
						var index = _data.FindIndex(b => b.Id1.ToString() == beacon.Id1.ToString());
						_data[index] = getDeviceBeacon(beacon);
					}
					else
					{
						newBeacons.Add(beacon);
					}
				}


				foreach (var beacon in newBeacons)
				{
					_data.Add(getDeviceBeacon(beacon));
				}

				if (newBeacons.Count > 1)
				{
					_data.Sort((x, y) => x.Distance.CompareTo(y.Distance));
				}


				if (bluManger != null)
					bluManger.updateBeaconsList(_data);

			});
		}



		void EnteredRegion(object sender, MonitorEventArgs e)
		{
			Task.Run(() =>
			{
				Toast.MakeText(Forms.Context, "You are entering ranfe of" + e.Region.Id1, ToastLength.Short).Show();
			});

		}

		void ExitedRegion(object sender, MonitorEventArgs e)
		{
			Task.Run(() =>
			{

				Toast.MakeText(Forms.Context, "You are went away :( of " + e.Region.Id1, ToastLength.Short).Show();

			});

		}

		void DeterminedStateForRegionComplete(object sender, MonitorEventArgs e)
		{
			Task.Run(() =>
			{
				Toast.MakeText(Forms.Context, "I have just switched from seeing/not seeing beacons: " + e.State, ToastLength.Short).Show();

			});

		}

		public bool BindService(Intent intent, IServiceConnection serviceConnection, [GeneratedEnum] Bind flags)
		{
			return true;
		}

		public void NotifyServiceConnection()
		{
			_beaconManager.SetForegroundBetweenScanPeriod(2000); // 5000 milliseconds

			_beaconManager.AddRangeNotifier(_rangeNotifier);
			_tagRegion = new AltBeaconOrg.BoundBeacon.Region("myUniqueBeaconId", Identifier.Parse("EBEFD083-70A2-47C8-9837-E7B5634DF524"), null, null);
			_tagRegion2 = new AltBeaconOrg.BoundBeacon.Region("myEmptyBeaconId", Identifier.Parse("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), null, null);
			_instanceRegion = new AltBeaconOrg.BoundBeacon.Region("myEmptyBeaconId", null, null, null);

			Identifier myBeaconNamespaceId = Identifier.Parse("0x5fca1f68372c8506c039");
			Identifier myBeaconInstanceId = Identifier.Parse("0x000000000000");


			_myeddystone = new AltBeaconOrg.BoundBeacon.Region("myeddystone", myBeaconNamespaceId, myBeaconInstanceId, null);

			_realeddystone2 = new AltBeaconOrg.BoundBeacon.Region("myeddystone",  Identifier.Parse("0xedd1ebeac04e5defa017"), Identifier.Parse("0x91794d1cf7ad"), null);

			_beaconManager.StartRangingBeaconsInRegion(_tagRegion);
			_beaconManager.StartRangingBeaconsInRegion(_tagRegion2);
			_beaconManager.StartRangingBeaconsInRegion(_myeddystone);
			_beaconManager.StartRangingBeaconsInRegion(_instanceRegion);
			_beaconManager.StartRangingBeaconsInRegion(_realeddystone2);


			_beaconManager.AddMonitorNotifier(_monitorNotifier);

			_beaconManager.StartMonitoringBeaconsInRegion(_tagRegion);
			_beaconManager.StartMonitoringBeaconsInRegion(_tagRegion2);
			_beaconManager.StartMonitoringBeaconsInRegion(_myeddystone);
			_beaconManager.StartMonitoringBeaconsInRegion(_instanceRegion);
			_beaconManager.StartMonitoringBeaconsInRegion(_realeddystone2);

		}

		public void UnbindService(IServiceConnection serviceConnection)
		{

		}

		BeaconDevice getDeviceBeacon(Beacon beacon)
		{
			BeaconDevice device = new BeaconDevice();

			if (beacon.ServiceUuid == 0xfeaa && beacon.BeaconTypeCode == 0x10)
			{
				// This is a Eddystone-URL frame
				String url = UrlBeaconUrlCompressor.Uncompress(beacon.Id1.ToByteArray());
				Console.WriteLine(url);
				device.url = url;
			}

			device.Distance = beacon.Distance;
			if (beacon.Id1 != null)
				device.Id1 = beacon.Id1.ToString();


			device.major = "" + beacon.Rssi;

			device.minor = "" + beacon.TxPower;



			device.minor = "";
			device.Rssi = beacon.Rssi;
			device.TxPower = beacon.TxPower;
			Console.WriteLine("Distance Length     :" + device.Distance);
			return device;
		}

		public void OnstopSearch()
		{

		}

		public void OnResumeSearch()
		{
			if (_beaconManager.IsBound(((IBeaconConsumer)Xamarin.Forms.Forms.Context)))

			{
				_beaconManager.SetBackgroundMode(false);

			}
		}

		public void OnBackGroundSearch()
		{

		}

		public void OnPauseSearch()
		{
			if (_beaconManager.IsBound((IBeaconConsumer)Xamarin.Forms.Forms.Context))

			{
				_beaconManager.SetBackgroundMode(true);
			}

		}

		public void OnClose()
		{
			_beaconManager.Unbind((IBeaconConsumer)Xamarin.Forms.Forms.Context);

		}
	}
}
