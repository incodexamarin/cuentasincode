﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using JSAPISdk;
using Knetik_Rest;
using KnetikIOT.Droid;
using StaffApp.Droid;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(LocationManger))]
namespace KnetikIOT.Droid
{
	public class LocationManger : Java.Lang.Object, ILocation, ILocationListener
	{

		ILocationHasChanged locationManger;
		LocationManager _locationManager;
		string _locationProvider;

		public Context ApplicationContext
		{
			get
			{
				return Forms.Context;
			}
		}

		public LocationManger()
		{

			InitializeLocationManager();

		}

		void InitializeLocationManager()
		{

			LocationManager mlocManager = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);

			bool enabled = mlocManager.IsProviderEnabled(LocationManager.GpsProvider);
			if (enabled == false)
			{
				return;
			}


			_locationManager = (LocationManager)Forms.Context.GetSystemService(Context.LocationService);

			Criteria criteriaForLocationService = new Criteria
			{
				Accuracy = Accuracy.Fine
			};

			IList<string> acceptableLocationProviders = _locationManager.GetProviders(criteriaForLocationService, false);

			if (acceptableLocationProviders.Any())
			{
				_locationProvider = acceptableLocationProviders.First();
				if (_locationManager != null)
					_locationManager.RequestLocationUpdates(_locationProvider, 2000, 0, this);
			}
			else
			{
				_locationProvider = null;
			}
		}


		public void OnLocationChanged(Android.Locations.Location location)
		{


			if (locationManger != null)
			{

				JSAPISdk.Location Userlocation = new JSAPISdk.Location();
				Userlocation.latitude = location.Latitude;
				Userlocation.longitude = location.Longitude;
				this.locationManger.locationChanged(Userlocation);
			}

		}

		public void registerLocationDelegate(ILocationHasChanged locationM)
		{
			this.locationManger = locationM;

		}

		public void verifyLocation()
		{

		}

		public void OnProviderDisabled(string provider)
		{

		}

		public void OnProviderEnabled(string provider)
		{
		}

		public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
		{
		}


		public void onLocationResume()
		{
			if (_locationManager != null && _locationProvider != null)
				_locationManager.RequestLocationUpdates(_locationProvider, 2000, 0, this);
		}

		public void RemoveUpdates()
		{
			if (_locationManager != null)
				_locationManager.RemoveUpdates(this);
		}

		public void OnConnected()
		{
			if (_locationManager != null)
				_locationManager.RequestLocationUpdates(_locationProvider, 2000, 0, this);
		}

		public double DistanceBetween(double startLatitude, double startLongitude, double endLatitude, double endLongitude)
		{
			Android.Locations.Location location1 = new Android.Locations.Location("crntlocation");
			location1.Latitude = startLatitude;
			location1.Longitude = startLongitude;

			Android.Locations.Location location2 = new Android.Locations.Location("newlocation");
			location2.Latitude = endLatitude;
			location2.Longitude = endLongitude;

			return location1.DistanceTo(location2);
		}

		public JSAPISdk.Location getCurrentLocation()
		{
			Criteria criteriaForLocationService = new Criteria
			{
				Accuracy = Accuracy.Fine
			};

			IList<string> acceptableLocationProviders = _locationManager.GetProviders(criteriaForLocationService, false);

			if (acceptableLocationProviders.Any())
			{
				_locationProvider = acceptableLocationProviders.First();
				if (_locationManager != null)
				{
					Android.Locations.Location realLocation = _locationManager.GetLastKnownLocation(_locationProvider);
					JSAPISdk.Location location = new JSAPISdk.Location();
					if (realLocation != null)
					{
						location.latitude = realLocation.Latitude;
						location.longitude = realLocation.Longitude;
					}
					return location;

				}
			}

			return new JSAPISdk.Location();

		}
	}
}
