﻿using System;
using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Cuentas.Droid.Services;
using Cuentas.Services.Abstractions;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;
using HockeyApp.Android.Utils;
using Prism;
using Prism.Ioc;
using Plugin.Permissions;
using Prism.Unity;
using Unity;

namespace Cuentas.Droid
{
    [Activity(Label = "Cuentas", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    [IntentFilter(new[] { Intent.ActionView },
        DataScheme = "cuentasapp",
        //DataHost = "invite",
        Categories = new[] { Intent.CategoryDefault, Intent.CategoryBrowsable })]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private const string HockeyappAppid = "ff81c6c6074d4d2189ead0291956935f";
        private App _pclApplication;    
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            HockeyLog.LogLevel = 3;
            CrashManager.Register(this, HockeyappAppid);
            UpdateManager.Register(this, HockeyappAppid);
            MetricsManager.Register(Application, HockeyappAppid);

            Rg.Plugins.Popup.Popup.Init(this, bundle);
            UserDialogs.Init(this);
            global::Xamarin.Forms.Forms.Init(this, bundle);
            _pclApplication = new App(new AndroidInitializer());
            LoadApplication(_pclApplication);
            //Need to execute this comand: adb shell am start -a android.intent.action.VIEW -c android.intent.category.BROWSABLE -d "url" to test app links
            string host = Intent?.Data?.EncodedAuthority;
            string data = Intent?.Data?.EncodedQuery;
            if (data != null)
                _pclApplication.HandleUrl(data);
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterInstance(typeof(IContactService), new ContactService());
        }
    }
}

