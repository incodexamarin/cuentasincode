﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Cuentas.Services.Abstractions;
using Cuentas.ViewModels.PageModels;

namespace Cuentas.Droid.Services
{
    public class ContactService : IContactService
    {
        public IEnumerable<PhoneContact> GetContactsByEmail()
        {           
            var uri = ContactsContract.CommonDataKinds.Email.ContentUri;            
            string[] projection =
            {
                ContactsContract.Contacts.InterfaceConsts.Id,
                ContactsContract.CommonDataKinds.Email.Address,
                ContactsContract.Contacts.InterfaceConsts.DisplayName                
            };
            return GetContacts(uri, projection);
          
        }

        public IEnumerable<PhoneContact> GetContactsByPhone()
        {
            var uri = ContactsContract.CommonDataKinds.Phone.ContentUri;
            string[] projection =
            {
                ContactsContract.Contacts.InterfaceConsts.Id,
                ContactsContract.CommonDataKinds.Phone.Number,
                ContactsContract.Contacts.InterfaceConsts.DisplayName               
            };
            return GetContacts(uri, projection);
        }

        private IEnumerable<PhoneContact> GetContacts(Android.Net.Uri uri, string[] projection)
        {
            var contactList = new List<PhoneContact>();
            using (var cursor = Application.Context.ContentResolver.Query(uri, projection, null, null, null))
            {
                if (cursor.MoveToFirst())
                {
                    do
                    {
                        try
                        {
                            contactList.Add(new PhoneContact
                            {
                                Contact = cursor.GetString(
                                cursor.GetColumnIndex(projection[1])),
                                FirstName = cursor.GetString(
                                cursor.GetColumnIndex(projection[2]))                               
                            });
                        }
                        catch
                        {
                            //something wrong with one contact, may be display name is completely empty, decide what to do
                        }
                    } while (cursor.MoveToNext());
                    cursor.Close();
                }
            }
            return contactList;
        }
    }
}