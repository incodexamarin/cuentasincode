﻿using Android.Content;
using Android.Graphics;
using Cuentas.CustomControls;
using Cuentas.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(UnderlineLabel), typeof(UnderlineLabelRenderer))]
namespace Cuentas.Droid.Renderers
{
    public class UnderlineLabelRenderer : LabelRenderer
    {
        public UnderlineLabelRenderer(Context context) : base(context)
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.PaintFlags = PaintFlags.UnderlineText;
            }

        }
    }
}