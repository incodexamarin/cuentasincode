﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using Cuentas.Models;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Cuentas.Views;
using Unity;
using Unity.ServiceLocation;
using Xamarin.Forms;

namespace Cuentas.Services
{
    public class AccesTokenApiService : AccessTokenApi, IAccessTokenApiService
    {
        private readonly IAccessTokenApi _aceAccessTokenApi;

        public AccesTokenApiService()
        {
            _aceAccessTokenApi = new AccessTokenApi();
        }

        public async Task ValidateTokenAsync()
        {
            try
            {
                var oauth = Settings.Current.Get<OAuthEntity>("OAuthToken");
                if ((DateTime.UtcNow - oauth.CreationDate).TotalSeconds > Convert.ToInt32(oauth.OAuth.ExpiresIn as string))
                {
                    Debug.WriteLine($"Old OAuth:" + Environment.NewLine + oauth.OAuth.ToString());
                    var result = await _aceAccessTokenApi.GetOAuthTokenAsync(
                        Constants.GrandTypes.refresh_token.ToString(), Constants.ClIENT_ID, "", "", "", "",
                        oauth.OAuth.RefreshToken);
                    Debug.WriteLine($"New OAuth:" + Environment.NewLine + result);
                    OAuthEntity response = new OAuthEntity(result, DateTime.UtcNow);
                    Settings.Current.Set("OAuthToken", response);
                    com.knetikcloud.Client.Configuration.Default.AccessToken = response.OAuth.AccessToken;
                }
                else
                {
                    com.knetikcloud.Client.Configuration.Default.AccessToken = oauth.OAuth.AccessToken;
                }
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast(e.Message, TimeSpan.FromSeconds(2));
                Application.Current.MainPage = new NavigationPage(new LoginPage());
            }
        }
    }
}