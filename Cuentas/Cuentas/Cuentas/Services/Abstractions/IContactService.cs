﻿using System.Collections.Generic;
using Cuentas.ViewModels.PageModels;

namespace Cuentas.Services.Abstractions
{
    public interface IContactService
    {
        IEnumerable<PhoneContact> GetContactsByPhone();
        IEnumerable<PhoneContact> GetContactsByEmail();
    }
}