﻿using System.Threading.Tasks;
using com.knetikcloud.Api;

namespace Cuentas.Services.Abstractions
{
    public interface IAccessTokenApiService : IAccessTokenApi
    {
        Task ValidateTokenAsync();
    }
}