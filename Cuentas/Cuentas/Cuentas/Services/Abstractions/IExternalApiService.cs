﻿using System.Net.Http;
using System.Threading.Tasks;
using Cuentas.Models.PendingRecipients;

namespace Cuentas.Services.Abstractions
{
    public interface IExternalApiService
    {
        Task PostInviteRecipientAsync(InviteRequest invite);
        Task PostAcceptInviteAsync(string uuid);
    }
}