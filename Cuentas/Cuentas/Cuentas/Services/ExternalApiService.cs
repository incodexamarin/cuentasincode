﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Models;
using Cuentas.Models.PendingRecipients;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Newtonsoft.Json;

namespace Cuentas.Services
{
    public class ExternalApiService : IExternalApiService
    {
        public async Task PostInviteRecipientAsync(InviteRequest invite)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                OAuthEntity oauthToken = Settings.Current.Get<OAuthEntity>("OAuthToken");
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + oauthToken?.OAuth.AccessToken);

                string json = JsonConvert.SerializeObject(invite, new JsonSerializerSettings
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                });

                HttpContent httpContent = new StringContent(json);

                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                httpContent.Headers.Add("x-knetikcloud-appid", "cuentas");
                HttpResponseMessage result = await httpClient.PostAsync(Constants.EXTERNAL_AMAZON_API, httpContent);

                if (!result.IsSuccessStatusCode)
                    throw new ApiException();
            }
        }

        public async Task PostAcceptInviteAsync(string uuid)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                OAuthEntity oauthToken = Settings.Current.Get<OAuthEntity>("OAuthToken");
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + oauthToken?.OAuth.AccessToken);
                HttpContent httpContent = new StringContent("");
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                httpContent.Headers.Add("x-knetikcloud-appid", "cuentas");
                HttpResponseMessage result =
                    await httpClient.PostAsync(Constants.EXTERNAL_AMAZON_API + uuid + "/accept", httpContent);
                if (!result.IsSuccessStatusCode)
                    throw new ApiException();
            }
        }
    }
}