﻿using Acr.UserDialogs;
using Prism.Navigation;
using Xamarin.Forms;

namespace Cuentas.Views
{
    public partial class FlyoutPage : MasterDetailPage//, IMasterDetailPageOptions
    {
        public FlyoutPage()
        {
            var x = typeof(Behaviors.EventHandlerBehavior); //fixed problems with Behaviors.Forms library
            UserDialogs.Instance.HideLoading();
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }
        //public bool IsPresentedAfterNavigation => Device.Idiom != TargetIdiom.Phone;
    }
}