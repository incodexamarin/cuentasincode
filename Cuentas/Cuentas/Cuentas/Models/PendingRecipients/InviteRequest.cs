﻿using System.Collections.Generic;
using System.ComponentModel;
using Cuentas.Models.GeneralModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cuentas.Models.PendingRecipients
{
    public class InviteRequest
    {
        [JsonProperty("recipient")]
        public string Recipient { get; set; }

        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public KindSearchRecipientsEnum Type { get; set; }

        [JsonProperty("unique_name")]
        public string UniqueName { get; set; }

        public InviteRequest()
        {
            UniqueName = "-recipients";
        }
    }
}