﻿using System.ComponentModel;
using Cuentas.Models.GeneralModels;
using Newtonsoft.Json;

namespace Cuentas.Models.PendingRecipients
{
    public class RecipientRequest
    {
        [JsonProperty("kind")]
        public KindOfSearchRecipients Kind { get; set; }

        [JsonProperty("email")]
        public StringValueType<string> Email { get; set; }

        [JsonProperty("mobile_number")]
        public StringValueType<string> MobilePhone { get; set; }

        public RecipientRequest()
        {
            Kind = new KindOfSearchRecipients();
            Email = new StringValueType<string>();
            MobilePhone = new StringValueType<string>();
        }
    }
}