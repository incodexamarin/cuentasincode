﻿using System.Collections.Generic;
using Cuentas.Models.GeneralModels;
using Cuentas.Models.PendingTransfers;
using Newtonsoft.Json;

namespace Cuentas.Models.PendingRecipients
{
    public class PendingRepicients : BasePendingList
    {                     
        [JsonProperty("values")]
        public List<MapRecipients> RecipientRequests { get; set; }

        public PendingRepicients()
        {
            RecipientRequests = new List<MapRecipients>();
        }
    }
}