﻿using System;
using System.Collections.Generic;
using Cuentas.Models.PendingTransfers;

namespace Cuentas.Models.PendingRecipients
{
    public class RecipientRequestCompare : IEqualityComparer<RecipientRequest>
    {
        public bool Equals(RecipientRequest x, RecipientRequest y)
        { 
            //TODO: need to add comparing of email 
            return x.MobilePhone.Value.Equals(y.MobilePhone.Value); //x.Email.Value.Equals(y.Email.Value);
        }

        public int GetHashCode(RecipientRequest obj)
        {
            return obj.GetHashCode();
        }
    }
}