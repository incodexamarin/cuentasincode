﻿using System.Collections.Generic;
using Cuentas.Models.PendingTransfers;
using Newtonsoft.Json;

namespace Cuentas.Models.PendingRecipients
{
    public class MapRecipients
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("map")]
        public RecipientRequest Recipient { get; set; }

        public MapRecipients(RecipientRequest recipientRequest)
        {
            Type = "map";
            Recipient = recipientRequest;
        }
    }
}