﻿using Cuentas.Models.GeneralModels;
using Newtonsoft.Json;

namespace Cuentas.Models.WalletSettings
{
    public class WalletSettings : BaseSettings
    {
        [JsonProperty("map")]
        public Map Map { get; set; }

        public WalletSettings()
        {
            Map = new Map();
        }
    }
}