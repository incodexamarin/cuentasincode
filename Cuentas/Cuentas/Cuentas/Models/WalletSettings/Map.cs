﻿using Cuentas.Models.GeneralModels;
using Newtonsoft.Json;

namespace Cuentas.Models.WalletSettings
{
    public class Map
    {
        [JsonProperty("usd_balance_min")]
        public IntValueType<int> UsdBalanceMin { get; set; }

        [JsonProperty("usd_email_notify")]
        public BoolValueType<bool> UsdEmailNotify { get; set; }

        [JsonProperty("usd_sms_notify")]
        public BoolValueType<bool> UsdSmsNotify { get; set; }

        public Map()
        {
            UsdBalanceMin = new IntValueType<int>();
            UsdEmailNotify = new BoolValueType<bool>();
            UsdSmsNotify = new BoolValueType<bool>();
        }
    }
}