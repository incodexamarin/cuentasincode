﻿using System;
using com.knetikcloud.Model;

namespace Cuentas.Models
{
    public class OAuthEntity
    {
        public OAuth2Resource OAuth { get; set; }
        public DateTime CreationDate { get; set; }

        public OAuthEntity(OAuth2Resource resource, DateTime creationDate)
        {
            OAuth = resource;
            CreationDate = creationDate;
        }
    }
}