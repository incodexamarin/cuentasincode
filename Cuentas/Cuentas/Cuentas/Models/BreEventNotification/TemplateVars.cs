﻿using System;
using Newtonsoft.Json;

namespace Cuentas.Models.BreEventNotification
{
    public class TemplateVars
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("sender")]
        public int? Sender { get; set; }
    }
}