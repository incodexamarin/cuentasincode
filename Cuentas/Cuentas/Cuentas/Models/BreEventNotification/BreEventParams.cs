﻿using Newtonsoft.Json;

namespace Cuentas.Models.BreEventNotification
{
    public class BreEventParams
    {
        [JsonProperty("email_subject")]
        public string EmailSubject { get; set; }

        [JsonProperty("email_template_id")]
        public string EmailTemplateId { get; set; }

        [JsonProperty("sms_template")]
        public string SmsTemplate { get; set; }

        [JsonProperty("template_vars")]
        public TemplateVars TemplateVars { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("user")]
        public int? User { get; set; }

        public BreEventParams(int? receiverId, string type, string smsTemplate)
        {
            User = receiverId;
            Type = $"transfer_{type}";
            EmailSubject = $"Cuentas Transfer {type}";
            EmailTemplateId = $"cuentas-transfer-{type}";
            SmsTemplate = smsTemplate;
        }
    }
}