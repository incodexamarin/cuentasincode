﻿namespace Cuentas.Models.GeneralModels
{
    public class LongValueType<T> : BaseValue<T>
    {
        public LongValueType()
        {
            Type = "long";
        }
    }
}