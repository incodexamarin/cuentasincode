﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cuentas.Models.GeneralModels
{
    public class KindOfSearchRecipients
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        [JsonConverter(typeof(StringEnumConverter))]
        public KindSearchRecipientsEnum Value { get; set; }

        public KindOfSearchRecipients()
        {
            Type = "text";
        }
    }
    public enum KindSearchRecipientsEnum
    {
        email,
        mobile_number,
        sms
    }
}