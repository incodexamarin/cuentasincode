﻿using Newtonsoft.Json;

namespace Cuentas.Models.GeneralModels
{
    public class BasePendingList
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        public BasePendingList()
        {
            Type = "list";
        }
    }
}