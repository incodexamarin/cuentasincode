﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Cuentas.Models.GeneralModels
{
    public class StateOfTransaction
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        [JsonConverter(typeof(StringEnumConverter))]
        public StateOfTransactionEnum Value { get; set; }

        public StateOfTransaction()
        {
            Type = "text";
        }
    }

    public enum StateOfTransactionEnum
    {
        pending,
        accepted,
        declined,
        request,
        complete,
        failed
    }
}