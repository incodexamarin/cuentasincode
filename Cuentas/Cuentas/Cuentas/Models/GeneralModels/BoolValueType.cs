﻿using System;
using Newtonsoft.Json;

namespace Cuentas.Models.GeneralModels
{
    public class BoolValueType<T> : BaseValue<T>
    {
        public BoolValueType()
        {
            Type = "boolean";
        }
    }
}