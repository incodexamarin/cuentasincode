﻿using Newtonsoft.Json;

namespace Cuentas.Models.GeneralModels
{
    public class BaseSettings
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        public BaseSettings()
        {
            Type = "map";
        }
    }
}