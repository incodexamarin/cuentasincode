﻿using Newtonsoft.Json;

namespace Cuentas.Models.GeneralModels
{
    public class BaseValue<T>
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public T Value { get; set; }
    }
}