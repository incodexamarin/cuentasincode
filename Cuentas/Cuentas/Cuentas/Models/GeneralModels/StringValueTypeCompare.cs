﻿using System.Collections.Generic;
using Cuentas.Models.PendingRecipients;

namespace Cuentas.Models.GeneralModels
{
    public class StringValueTypeCompare : IEqualityComparer<StringValueType<string>>
    {
        public bool Equals(StringValueType<string> x, StringValueType<string> y)
        {
            return x.Value.Equals(y.Value); 
        }

        public int GetHashCode(StringValueType<string> obj)
        {
            return obj.GetHashCode();
        }
    }
}