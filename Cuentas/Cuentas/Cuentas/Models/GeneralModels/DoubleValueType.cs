﻿namespace Cuentas.Models.GeneralModels
{
    public class DoubleValueType<T> : BaseValue<T>
    {
        public DoubleValueType()
        {
            Type = "double";
        }
    }
}