﻿using Newtonsoft.Json;

namespace Cuentas.Models.GeneralModels
{
    public class IntValueType<T> : BaseValue<T>
    {
        public IntValueType()
        {
            Type = "integer";
        }
    }
}