﻿namespace Cuentas.Models.GeneralModels
{
    public class StringValueType<T> : BaseValue<T>
    {
        public StringValueType()
        {
            Type = "text";
        }
    }
}