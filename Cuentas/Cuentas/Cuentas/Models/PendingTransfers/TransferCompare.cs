﻿using System;
using System.Collections.Generic;

namespace Cuentas.Models.PendingTransfers
{
    public class TransferCompare : IEqualityComparer<Transfer>
    {
        public bool Equals(Transfer x, Transfer y)
        {
            return x.CreationDate.Value == y.CreationDate.Value && Math.Abs(x.Amount.Value - y.Amount.Value) <= 0.0 &&
                   x.From.Value == y.From.Value && x.To.Value == y.To.Value && x.State.Value == y.State.Value;

        }

        public int GetHashCode(Transfer obj)
        {
            return obj.GetHashCode();
        }
    }
}