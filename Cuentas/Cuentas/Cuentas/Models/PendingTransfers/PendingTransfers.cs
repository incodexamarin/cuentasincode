﻿using System.Collections.Generic;
using Cuentas.Models.GeneralModels;
using Newtonsoft.Json;

namespace Cuentas.Models.PendingTransfers
{
    public class PendingTransfers : BasePendingList
    {
        [JsonProperty("values")]
        public List<MapTransfer> Transfers { get; set; }

        public PendingTransfers()
        {
            Transfers = new List<MapTransfer>();
        }
    }
}