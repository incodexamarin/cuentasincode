﻿using Cuentas.Models.GeneralModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Cuentas.Models.PendingTransfers
{
    public class Transfer
    {

        [JsonProperty("creation_date")]
        public LongValueType<long> CreationDate { get; set; }
        [JsonProperty("to")]
        public IntValueType<int> To { get; set; }
        [JsonProperty("from")]
        public IntValueType<int> From { get; set; }
        [JsonProperty("amount")]
        public DoubleValueType<double> Amount { get; set; }
        [JsonProperty("state")]
        public StateOfTransaction State { get; set; }

        public Transfer()
        {
            CreationDate = new LongValueType<long>();
            To = new IntValueType<int>();
            From = new IntValueType<int>();
            Amount = new DoubleValueType<double>();
            State = new StateOfTransaction();
        }
    }
}