﻿using Newtonsoft.Json;

namespace Cuentas.Models.PendingTransfers
{
    public class MapTransfer
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("map")]
        public Transfer Transfer { get; set; }

        public MapTransfer(Transfer transfer)
        {
            Type = "map";
            Transfer = transfer;
        }
    }
}