﻿using Cuentas.Models.GeneralModels;
using Newtonsoft.Json;

namespace Cuentas.Models.AlertSettings
{
    public class AlertSettings : BaseSettings
    {
        [JsonProperty("map")]
        public Map Map { get; set; }

        public AlertSettings()
        {
            Map = new Map();
        }
    }
}