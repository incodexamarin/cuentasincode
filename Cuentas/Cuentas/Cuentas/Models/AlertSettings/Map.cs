﻿using Cuentas.Models.GeneralModels;
using Newtonsoft.Json;

namespace Cuentas.Models.AlertSettings
{
    public class Map
    {
        [JsonProperty("credit_transaction_min")]
        public IntValueType<int> CreditTransactionMin { get; set; }

        [JsonProperty("debit_transaction_min")]
        public IntValueType<int> DebitTransactionMin { get; set; }

        [JsonProperty("debit_transaction_email_notify")]
        public BoolValueType<bool> DebitTransactionEmailNotify { get; set; }

        [JsonProperty("debit_transaction_sms_notify")]
        public BoolValueType<bool> DebitTransactionSmsNotify { get; set; }

        [JsonProperty("credit_transaction_email_notify")]
        public BoolValueType<bool> CreditTransactionEmailNotify { get; set; }

        [JsonProperty("credit_transaction_sms_notify")]
        public BoolValueType<bool> CreditTransactionSmsNotify { get; set; }

        [JsonProperty("address_change_email_notify")]
        public BoolValueType<bool> AddressChangeEmailNotify { get; set; }

        [JsonProperty("address_change_sms_notify")]
        public BoolValueType<bool> AddressChangeSmsNotify { get; set; }

        [JsonProperty("password_change_email_notify")]
        public BoolValueType<bool>PasswordChangeEmailNotify { get; set; }

        [JsonProperty("password_change_sms_notify")]
        public BoolValueType<bool> PasswordChangeSmsNotify { get; set; }

        [JsonProperty("payment_profile_email_notify")]
        public BoolValueType<bool> PaymentProfileEmailNotify { get; set; }

        [JsonProperty("payment_profile_sms_notify")]
        public BoolValueType<bool> PaymentProfileSmsNotify { get; set; }

        [JsonProperty("payment_profile_remove_email_notify")]
        public BoolValueType<bool> PaymentProfileRemoveEmailNotify { get; set; }

        [JsonProperty("payment_profile_remove_sms_notify")]
        public BoolValueType<bool> PaymentProfileRemoveSmsNotify { get; set; }

        [JsonProperty("other_profile_fields_change_email_notify")]
        public BoolValueType<bool> OtherProfileFieldsChangeEmailNotify { get; set; }

        [JsonProperty("other_profile_fields_change_sms_notify")]
        public BoolValueType<bool> OtherProfileFieldsChangeSmsNotify { get; set; }

        [JsonProperty("user_settings_email_notify")]
        public BoolValueType<bool> UserSettingsEmailNotify { get; set; }

        [JsonProperty("user_settings_sms_notify")]
        public BoolValueType<bool> UserSettingsSmsNotify { get; set; }

        [JsonProperty("scheduled_payments_email_notify")]
        public BoolValueType<bool> ScheduledPaymentsEmailNotify { get; set; }

        [JsonProperty("scheduled_payments_sms_notify")]
        public BoolValueType<bool> ScheduledPaymentsSmsNotify { get; set; }

        [JsonProperty("recipient_request_email_notify")]
        public BoolValueType<bool> RecipientRequestEmailNotify { get; set; }

        [JsonProperty("recipient_request_sms_notify")]
        public BoolValueType<bool> RecipientRequestSmsNotify { get; set; }

        public Map()
        {
            RecipientRequestSmsNotify = new BoolValueType<bool>();
            RecipientRequestEmailNotify = new BoolValueType<bool>();
            ScheduledPaymentsSmsNotify = new BoolValueType<bool>();
            ScheduledPaymentsEmailNotify = new BoolValueType<bool>();
            UserSettingsSmsNotify = new BoolValueType<bool>();
            UserSettingsEmailNotify = new BoolValueType<bool>();
            OtherProfileFieldsChangeSmsNotify = new BoolValueType<bool>();
            OtherProfileFieldsChangeEmailNotify = new BoolValueType<bool>();
            PaymentProfileEmailNotify = new BoolValueType<bool>();
            PaymentProfileSmsNotify = new BoolValueType<bool>();
            PaymentProfileRemoveSmsNotify = new BoolValueType<bool>();
            PaymentProfileRemoveEmailNotify = new BoolValueType<bool>();
            PasswordChangeSmsNotify = new BoolValueType<bool>();
            PasswordChangeEmailNotify = new BoolValueType<bool>();
            AddressChangeSmsNotify = new BoolValueType<bool>();
            AddressChangeEmailNotify = new BoolValueType<bool>();
            CreditTransactionSmsNotify = new BoolValueType<bool>();
            CreditTransactionEmailNotify = new BoolValueType<bool>();
            DebitTransactionSmsNotify = new BoolValueType<bool>();
            DebitTransactionEmailNotify = new BoolValueType<bool>();
            CreditTransactionMin = new IntValueType<int>();
            DebitTransactionMin = new IntValueType<int>();
        }
    }
}