﻿using System;
using System.Diagnostics;
using Acr.Settings;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Models;
using Cuentas.Services;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Cuentas.ViewModels;
using Cuentas.Views;
using Prism;
using Prism.Events;
using Prism.Ioc;
using Prism.Navigation;
using Prism.Plugin.Popups;
using Prism.Unity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Cuentas
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer)
        {
            Navigation = NavigationService;
        }
        
        public INavigationService Navigation { get; set; }
        protected override void OnInitialized()
        {
            try
            {
                Xamarin.Forms.DataGrid.DataGridComponent.Init(); //initializing of the DataGrid
                InitializeComponent();

                var oauth2 = Settings.Current.Get<OAuthEntity>("OAuthToken");
                var currentUser = Settings.Current.Get<UserResource>("CurrentUser");
                if (oauth2 != null && currentUser != null)
                {
                    Container.Resolve<IAccessTokenApiService>().ValidateTokenAsync();
                    NavigationService.NavigateAsync("Index/Navigation/Home");
                }
                else
                {
                    NavigationService.NavigateAsync("Navigation/Login");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
        }

        protected override void OnResume()
        {
            var oauth2 = Settings.Current.Get<OAuthEntity>("OAuthToken");
            var currentUser = Settings.Current.Get<UserResource>("CurrentUser");
            if (oauth2 != null && currentUser != null)
            {
                Container.Resolve<IAccessTokenApiService>().ValidateTokenAsync();
            }
        }

        public void HandleUrl(string data)
        {
            if (data != null && data.Contains("invite_token="))
            {
                var uuid = data.Replace("invite_token=", "");
                Settings.Current.Set("InviteToken", uuid);
            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            Configuration.Default.BasePath = Constants.KNETIK_JSPAI_URL;
            
            containerRegistry.RegisterPopupNavigationService();

            containerRegistry.RegisterForNavigation<NavigationPage>("Navigation");
            containerRegistry.RegisterForNavigation<LoginPage>("Login");
            containerRegistry.RegisterForNavigation<RegistrationPage>("Registration");
            containerRegistry.RegisterForNavigation<FlyoutPage>("Index");
            containerRegistry.RegisterForNavigation<RecipientsPage>("Recipients");
            containerRegistry.RegisterForNavigation<AlertSettingsPage>("AlertSettings");
            containerRegistry.RegisterForNavigation<HomePage>("Home");
            containerRegistry.RegisterForNavigation<TransfersPage>("Transfers");
            containerRegistry.RegisterForNavigation<CompleteTransfersListPage>("CompleteTransfers");
            containerRegistry.RegisterForNavigation<PendingTransfersPage>("PendingTransfers");
            containerRegistry.RegisterForNavigation<WebViewPage>("WebView");
            containerRegistry.RegisterForNavigation<WalletSettingsPage>("WalletSettings");
            containerRegistry.RegisterForNavigation<ReferralPage>("Referral");
            containerRegistry.RegisterForNavigation<RequestPopupPage>("RequestSearch");
            containerRegistry.RegisterForNavigation<ForgotPwdPopupPage>("ForgotPassword");
            containerRegistry.RegisterForNavigation<InviteSuccessPopupPage>("InviteSuccess");
            containerRegistry.RegisterForNavigation<ContactListPopupPage>("ContactList");

            //containerRegistry.RegisterInstance(typeof(IAccessTokenApi), new AccessTokenApi());
            containerRegistry.RegisterInstance(typeof(IAccessTokenApiService), new AccesTokenApiService());
            containerRegistry.RegisterInstance(typeof(IUsersApi), new UsersApi());
            containerRegistry.RegisterInstance(typeof(IUsersGroupsApi), new UsersGroupsApi());
            containerRegistry.RegisterInstance(typeof(IPaymentsWalletsApi), new PaymentsWalletsApi());
            containerRegistry.RegisterInstance(typeof(IBRERuleEngineEventsApi), new BRERuleEngineEventsApi());
            containerRegistry.RegisterInstance(typeof(ISettings), Settings.Current);
            containerRegistry.RegisterInstance(typeof(IContentArticlesApi), new ContentArticlesApi());
            containerRegistry.RegisterInstance(typeof(IExternalApiService), new ExternalApiService());
        }
    }
}

