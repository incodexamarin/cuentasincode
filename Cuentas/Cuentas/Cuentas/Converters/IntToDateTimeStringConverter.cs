﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Cuentas.Converters
{
    public class IntToDateTimeStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string date = null;
            if (value != null)
            {
                DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds((long) value);
                DateTime dateTime = dateTimeOffset.UtcDateTime;
                date = dateTime.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            }
            return date;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? 0 : 1;
        }
    }
}