﻿using System;
using System.Globalization;
using Cuentas.Models.GeneralModels;
using Xamarin.Forms;

namespace Cuentas.Converters
{
    public class StatusToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var color = Color.White;
            if (value != null)
            {
                var state = (StateOfTransactionEnum)value;
                switch (state)
                {
                    case StateOfTransactionEnum.accepted:
                        return Color.LightGreen;
                    case StateOfTransactionEnum.declined:
                        return Color.LightCoral;
                    default:
                        return Color.White;
                }
            }
            return color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var state = (StateOfTransactionEnum)value;
            switch (state)
            {
                case StateOfTransactionEnum.accepted:
                    return Color.LightGreen;
                case StateOfTransactionEnum.declined:
                    return Color.LightCoral;
                default:
                    return Color.White;
            }
        }
    }
}