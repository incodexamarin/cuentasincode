﻿
using System;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;


namespace Cuentas.Utils
{
    public static class ApiExceptionExtensions
    {

        public static async Task<OAuth2Resource> UpdateAccessToken(this ApiException exception)
        {
            try
            {
                OAuth2Resource oauthToken = Settings.Current.Get<OAuth2Resource>("OAuthToken");
                IAccessTokenApi accessTokenApi = new AccessTokenApi(Constants.KNETIK_JSPAI_URL);
                var result = await accessTokenApi.GetOAuthTokenAsync(Constants.GrandTypes.refresh_token.ToString(),
                    Constants.ClIENT_ID, "", "", "", oauthToken.AccessToken, oauthToken.RefreshToken);
                Settings.Current.Set("OAuthToken", result);
                return result;
            }
            catch (Exception e)
            {
                UserDialogs.Instance.Toast(e.Message, TimeSpan.FromSeconds(2));
                return null;
            }
        }
    }
}