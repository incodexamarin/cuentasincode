﻿namespace Cuentas.Utils
{
    public static class Constants
    {
        public static string SECRET_KEY = "";
        public static string CLIENT_KEY = "PaymentAndNotificationMobileClient";//"mobiClient"; //PaymentAndNotificationMobileClient is the test client
        public static string ClIENT_ID = "PaymentAndNotificationMobileClient";//"mobiClient";

        public static string KNETIK_JSPAI_URL = "https://cuentas.devsandbox.knetikcloud.com/";
        public static string KNETIK_NOTIFICATION_URL = "https://cuentas.devsandbox.knetikcloud.com/";
        public static string EXTERNAL_AMAZON_API = "https://q05yf0lw45.execute-api.us-east-1.amazonaws.com/dev/users/invite/";//"https://o2aje1cklk.execute-api.us-east-1.amazonaws.com/development/users/invite/";


        //Regular expressions
        public static string EmailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        public static string HowItWorksId = "5a3917e2d644c9688b8e0c03";
        public static string PromoPageId = "5a3917fbf8f1f144ba0e7e90";
        public static string LegalId = "5a391810f8f1f144ba0e7ec5";
        public static string HelpWizardId = "5a391828f8f1f144ba0e7ef3";
        public static string FaqsId = "5a391846d644c9688b8e0d38";
        public static string ContactId = "5a391855d644c9688b8e0da9";
        public static string LegalPageId = "5a391869f8f1f144ba0e8004";
        public static string FeesAndLimitsId = "5a39187ff8f1f144ba0e801e";


        public enum GrandTypes
        {
            password,
            client_credentials,
            facebook,
            google,
            refresh_token
        }
    }
}