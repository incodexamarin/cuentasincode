﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using com.knetikcloud.Model;
using Cuentas.Models.GeneralModels;
using Cuentas.Models.PendingRecipients;
using Cuentas.Models.PendingTransfers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Cuentas.Utils
{
    public class PropertyWrapper
    {
        public UserResource CreateOrUpdatePendingTransfersList(UserResource user, Transfer currentTransfer)
        {
            PendingTransfers transfers = new PendingTransfers();
            if (user.AdditionalProperties == null) //checking if the additional properties isnot empty
            {
                JObject pendingTransfersObject = SerializeUpdatingPendingList(ref transfers, currentTransfer);
                user.AdditionalProperties = new Dictionary<string, JObject> { { "pending_transfers", pendingTransfersObject } };
            }
            else
            {
                if (!user.AdditionalProperties.ContainsKey("pending_transfers")) //checking if the additional properties contain the necessary property
                {
                    JObject pendingTransfersObject = SerializeUpdatingPendingList(ref transfers, currentTransfer);
                    user.AdditionalProperties.Add("pending_transfers", pendingTransfersObject);
                }
                else
                {
                    //transfers = DesirializeList("pending_transfers", user) as PendingTransfers;
                    JObject pendingTransfers = user.AdditionalProperties["pending_transfers"];
                    transfers = JsonConvert.DeserializeObject<PendingTransfers>(
                        pendingTransfers.ToString(),
                        new JsonSerializerSettings()
                        {
                            DateParseHandling = DateParseHandling.None,
                            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                            NullValueHandling = NullValueHandling.Ignore
                        });

                    if (transfers != null && transfers.Transfers.Any(transfer => new TransferCompare().Equals(transfer.Transfer, currentTransfer))) return null; //checking if the current transfer exists at the pending list
                    JObject pendingTransfersObject = SerializeUpdatingPendingList(ref transfers, currentTransfer);
                    user.AdditionalProperties["pending_transfers"] = pendingTransfersObject;
                }
            }
            return user;
        }

        public UserResource CreateOrUpdatePendingRecipientsList(UserResource user, RecipientRequest currentRequest)
        {
            PendingRepicients recipients = new PendingRepicients();
            if (user.AdditionalProperties == null) //checking if the additional properties isnot empty
            {
                JObject pendingRecipientsObject = SerializeUpdatingRecipientsList(ref recipients, currentRequest);
                user.AdditionalProperties = new Dictionary<string, JObject> { { "pending_recipients", pendingRecipientsObject } };
            }
            else
            {
                if (!user.AdditionalProperties.ContainsKey("pending_recipients")) //checking if the additional properties contain the necessary property
                {
                    JObject pendingRecipientsObject = SerializeUpdatingRecipientsList(ref recipients, currentRequest);
                    user.AdditionalProperties.Add("pending_recipients", pendingRecipientsObject);
                }
                else
                {
                    //recipients = DesirializeList("pending_recipients", user) as PendingRepicients;
                    JObject pendingTransfers = user.AdditionalProperties["pending_recipients"];
                    recipients = JsonConvert.DeserializeObject<PendingRepicients>(
                        pendingTransfers.ToString(),
                        new JsonSerializerSettings()
                        {
                            DateParseHandling = DateParseHandling.None,
                            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                            NullValueHandling = NullValueHandling.Ignore
                        });

                    if (recipients?.RecipientRequests != null && recipients.RecipientRequests.Any(request => new RecipientRequestCompare().Equals(request.Recipient, currentRequest))) return null; //checking if the current transfer exists at the pending list
                    JObject pendingRecipientsObject = SerializeUpdatingRecipientsList(ref recipients, currentRequest);
                    user.AdditionalProperties["pending_recipients"] = pendingRecipientsObject;
                }
            }
            return user;
        }
        private BasePendingList DesirializeList(string nameList, UserResource user)
        {
            JObject pendingTransfers = user.AdditionalProperties[nameList];
            BasePendingList lists = JsonConvert.DeserializeObject<BasePendingList>(
                pendingTransfers.ToString(),
                new JsonSerializerSettings()
                {
                    DateParseHandling = DateParseHandling.None,
                    MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                });
            return lists;
        }

        private JObject SerializeUpdatingPendingList(ref PendingTransfers transfers, Transfer currentTransfer)
        {
            transfers.Transfers.Add(new MapTransfer(currentTransfer));
            JObject pendingTransfersObject = JObject.FromObject(transfers);
            return pendingTransfersObject;
        }

        private JObject SerializeUpdatingRecipientsList(ref PendingRepicients pendingRepicients, RecipientRequest currentRequest)
        {
            pendingRepicients.RecipientRequests.Add(new MapRecipients(currentRequest));
            JObject pendingRecipientsObject = JObject.FromObject(pendingRepicients);
            return pendingRecipientsObject;
        }
    }
}