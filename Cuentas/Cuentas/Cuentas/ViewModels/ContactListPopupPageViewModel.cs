﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using com.knetikcloud.Model;
using Cuentas.Services.Abstractions;
using Cuentas.ViewModels.PageModels;
using Prism.Navigation;

namespace Cuentas.ViewModels
{
	public class ContactListPopupPageViewModel : BaseViewModel
	{
	    public ContactListPopupPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService) : base(navigationService, accessTokenApiService)
	    {
	    }

	    private DelegateCommand<PhoneContact> _itemTappedCommand;
	    public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand =
	                                             new DelegateCommand<PhoneContact>(OnItemTappedCommandExecuted));

        private ObservableCollection<PhoneContact> _contactList;
	    public ObservableCollection<PhoneContact> ContactList
        {
	        get { return _contactList; }
	        set { SetProperty(ref _contactList, value); }
	    }

	    private PhoneContact _selectedContactItem;
	    public PhoneContact SelectedContactItem
        {
	        get { return _selectedContactItem; }
	        set { SetProperty(ref _selectedContactItem, value); }
	    }

	    private bool _isEmail;

	    public override void OnNavigatingTo(NavigationParameters parameters)
	    {
	        if (parameters.ContainsKey("contacts") && parameters.ContainsKey("contactType"))
	        {
	            _isEmail = (bool) parameters["contactType"];
	            List<PhoneContact> list = (List<PhoneContact>) parameters["contacts"];
	            ContactList = new ObservableCollection<PhoneContact>(list);
	        }
	        else
	            ContactList = new ObservableCollection<PhoneContact>();
	    }

	    private async void OnItemTappedCommandExecuted(PhoneContact contact)
	    {
	        var paramerter = new NavigationParameters { { "contact", contact }, { "contactType", _isEmail } }; //the contact parameter indicates whether the destination is Email or Phone
	        await _navigationService.GoBackAsync(paramerter);
        }
	}
}
