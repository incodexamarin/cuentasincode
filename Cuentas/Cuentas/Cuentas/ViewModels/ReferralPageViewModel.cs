﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Models.GeneralModels;
using Cuentas.Models.PendingRecipients;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Cuentas.ViewModels.PageModels;
using HashidsNet;
using Newtonsoft.Json.Linq;
using Plugin.Permissions.Abstractions;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace Cuentas.ViewModels
{
    public class ReferralPageViewModel : BaseViewModel
    {
        private readonly IExternalApiService _externalApiService;
        private readonly IPageDialogService _pageDialogService;
        private readonly IUsersApi _usersApi;
        private readonly IContactService _contactService;
        public ReferralPageViewModel(INavigationService navigationService, IContactService contactService, IUsersApi usersApi, IPageDialogService pageDialogService, IExternalApiService externalApiService, IAccessTokenApiService accessTokenApiService) : base(navigationService, accessTokenApiService)
        {
            _externalApiService = externalApiService;
            _usersApi = usersApi;

            _contactService = contactService;
            _pageDialogService = pageDialogService;
        }

        //Should update when ApiService is added to ViewModel 
        private async Task CheckAccessToken()
        {
            await ValidationOAuthToken();
            string accessToken = Configuration.Default.AccessToken;
            _usersApi.Configuration.AccessToken = accessToken;
        }

        private DelegateCommand _sendCodeCommand;

        public DelegateCommand SendCodeCommand =>
            _sendCodeCommand ?? (_sendCodeCommand =
                new DelegateCommand(OnSendCodeCommandExecuted, SendCodeCommandCanExecute)
                    .ObservesProperty(() => ReferralCode));


        private DelegateCommand _sendEmailCommand;

        public DelegateCommand SendEmailCommand =>
            _sendEmailCommand ?? (_sendEmailCommand =
                new DelegateCommand(OnSendEmailCommandExecuted, SendEmailCommandCanExecute)
                    .ObservesProperty(() => Email));

        private DelegateCommand _sendPhoneCommand;

        public DelegateCommand SendPhoneCommand =>
            _sendPhoneCommand ?? (_sendPhoneCommand =
                new DelegateCommand(OnSendPhoneCommandExecuted, SendPhoneCommandCanExecute)
                    .ObservesProperty(() => Phone));

        private DelegateCommand _chooseContactByNumberCommand;

        public DelegateCommand ChooseContactByNumberCommand =>
            _chooseContactByNumberCommand ?? (_chooseContactByNumberCommand =
                new DelegateCommand(OnChooseContactByNumberCommandExecuted));

        private DelegateCommand _chooseContactByEmailCommand;

        public DelegateCommand ChooseContactByEmailCommand =>
            _chooseContactByEmailCommand ?? (_chooseContactByEmailCommand =
                new DelegateCommand(OnChooseContactByEmailCommandExecuted));

        private bool _isReferral;
        public bool IsReferral
        {
            get { return _isReferral; }
            set { SetProperty(ref _isReferral, value); }
        }

        private string _referralCode;
        public string ReferralCode
        {
            get { return _referralCode; }
            set { SetProperty(ref _referralCode, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { SetProperty(ref _phone, value); }
        }

        private UserResource _user;
        public UserResource User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            User = Settings.Current.Get<UserResource>("CurrentUser");
            IsReferral = User?.AdditionalProperties != null && User.AdditionalProperties.ContainsKey("referred_by");
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("contact") && parameters.ContainsKey("contactType"))
            {
                PhoneContact contact = (PhoneContact) parameters["contact"];
                if ((bool) parameters["contactType"])
                    Email = contact.Contact;
                else
                    Phone = contact.Contact;
            }
            else if (parameters.ContainsKey("invite"))
                Email = Phone = string.Empty;
        }

        private async void OnChooseContactByNumberCommandExecuted()
        {
            try
            {
                await CheckContactPermission();

                var contactList = _contactService.GetContactsByPhone();
                if (contactList == null) return;
                var paramerter = new NavigationParameters { { "contacts", contactList }, { "contactType", false } };
                await _navigationService.NavigateAsync("ContactList", paramerter);
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }

        private async void OnChooseContactByEmailCommandExecuted()
        {
            try
            {
                await CheckContactPermission();

                var contactList = _contactService.GetContactsByEmail();
                if (contactList == null) return;
                var paramerter = new NavigationParameters { { "contacts", contactList }, { "contactType", true } };
                await _navigationService.NavigateAsync("ContactList", paramerter);
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }

        private async void OnSendCodeCommandExecuted()
        {
            try
            {
                await CheckAccessToken();
               
                var hashids = new Hashids("0*nCHWG8h7@*hQY&J", 6, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
                var id = hashids.Decode(ReferralCode.ToUpperInvariant());
                if (id.Length > 0 && !IsReferral)
                {
                    UserDialogs.Instance.ShowLoading("Loading...");
                    await _usersApi.GetUserAsync(id[0].ToString());
                    JObject referralJson = JObject.FromObject(new IntValueType<int> {Value = id[0] });

                    User.AdditionalProperties = User.AdditionalProperties ?? new Dictionary<string, JObject>();
                    User.AdditionalProperties.Add("referred_by", referralJson);
                    await _usersApi.UpdateUserAsync("me", User);
                    User = await _usersApi.GetUserAsync("me");
                    Settings.Current.Set("CurrentUser", User);
                    IsReferral = true;
                    UserDialogs.Instance.Toast("Thank you!", TimeSpan.FromSeconds(2));
                }
                else
                    UserDialogs.Instance.Toast("Referral code is incorrect!", TimeSpan.FromSeconds(2));
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        private async void OnSendEmailCommandExecuted()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");
                await _externalApiService.PostInviteRecipientAsync(new InviteRequest
                {
                    Recipient = Email,
                    Type = KindSearchRecipientsEnum.email,
                    UniqueName = "referral" //instead of -recipients
                });
                var paramerter = new NavigationParameters {{"destination", Email}};
                await _navigationService.NavigateAsync("InviteSuccess", paramerter);
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        private async void OnSendPhoneCommandExecuted()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");
                await _externalApiService.PostInviteRecipientAsync(new InviteRequest
                {
                    Recipient = Phone,
                    Type = KindSearchRecipientsEnum.sms,
                    UniqueName = "referral"                                 //instead of -recipients
                });
                var paramerter = new NavigationParameters { { "destination", Phone } };
                await _navigationService.NavigateAsync("InviteSuccess", paramerter);
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }


        private async Task CheckContactPermission()
        {
            try
            {
                var status =
                    await Plugin.Permissions.CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Contacts);
                if (status == PermissionStatus.Denied)
                {
                    var result = await UserDialogs.Instance.ConfirmAsync(
                        "You have already denied access to contact data.\r\nYou can change it from Settings.",
                        "Warning",
                        "Settings", "Cancel");
                    if (result)
                    {
                        Device.OpenUri(new Uri("app-settings:"));
                    }
                }
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }

        private bool SendCodeCommandCanExecute() => !string.IsNullOrWhiteSpace(ReferralCode);

        private bool SendEmailCommandCanExecute() => !string.IsNullOrWhiteSpace(Email) &&
                                                     !string.IsNullOrEmpty(Constants.EmailRegex) && Regex.IsMatch(Email,
                                                         Constants.EmailRegex, RegexOptions.IgnoreCase) && IsNotBusy;

        private bool SendPhoneCommandCanExecute() => !string.IsNullOrWhiteSpace(Phone) && IsNotBusy;
    }
}
