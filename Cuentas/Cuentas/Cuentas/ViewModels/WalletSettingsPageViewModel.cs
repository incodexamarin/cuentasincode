﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Models.AlertSettings;
using Cuentas.Models.WalletSettings;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using Prism.Services;

namespace Cuentas.ViewModels
{
	public class WalletSettingsPageViewModel : BaseViewModel
	{
        private readonly IPageDialogService _pageDialogService;
        private readonly IUsersApi _usersApi;

	    public WalletSettingsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
	        IAccessTokenApiService accessTokenApiService, IUsersApi usersApi) :
	        base(navigationService, accessTokenApiService)
	    {
	        _pageDialogService = pageDialogService;
	        _usersApi = usersApi;
	    }

	    //Should update when ApiService is added to ViewModel 
	    private async Task CheckAccessToken()
	    {
	        await ValidationOAuthToken();
	        string accessToken = Configuration.Default.AccessToken;
	        _usersApi.Configuration.AccessToken = accessToken;
	    }

        private DelegateCommand _saveWalletSettingsCommand;
        public DelegateCommand SaveWalletSettingsCommand => _saveWalletSettingsCommand ?? (_saveWalletSettingsCommand =
                                                     new DelegateCommand(OnSaveWalletSettingsCommandExecuted));


        private UserResource _user;
        public UserResource User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private WalletSettings _walletSettings;
        public WalletSettings WalletSettings
        {
            get { return _walletSettings; }
            set { SetProperty(ref _walletSettings, value); }
        }

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            try
            {
                User = Settings.Current.Get<UserResource>("CurrentUser");
                WalletSettings = new WalletSettings();
                if (User.AdditionalProperties != null && User.AdditionalProperties.ContainsKey("wallet_settings"))
                {
                    JObject settings = User.AdditionalProperties["wallet_settings"];
                    WalletSettings = JsonConvert.DeserializeObject<WalletSettings>(settings.ToString(),
                        new JsonSerializerSettings()
                        {
                            DateParseHandling = DateParseHandling.None,
                            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                            NullValueHandling = NullValueHandling.Ignore
                        });
                }
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }

        private async void OnSaveWalletSettingsCommandExecuted()
        {
            try
            {
                await CheckAccessToken();

                UserDialogs.Instance.ShowLoading("Loading...");
                JObject settingsObject = JObject.FromObject(WalletSettings);
                if (User.AdditionalProperties == null)
                {
                    User.AdditionalProperties = new Dictionary<string, JObject> {{"wallet_settings", settingsObject}};
                }
                else
                {
                    User.AdditionalProperties.Remove("wallet_settings");
                    User.AdditionalProperties.Add("wallet_settings", settingsObject);
                    JObject b = JObject.FromObject(User);
                    Debug.WriteLine(b.ToString());
                }
                await _usersApi.UpdateUserAsync(Convert.ToString(User.Id), User);
                User = await _usersApi.GetUserAsync("me");
                Settings.Current.Set("CurrentUser", User);
                UserDialogs.Instance.HideLoading();
            }
            //catch (ApiException e)
            //{
            //    if (e.ErrorCode == 401)
            //    {
            //        OAuth2Resource oauth = await e.UpdateAccessToken();
            //        _usersApi.Configuration.AccessToken = oauth.AccessToken;
            //    }
            //}
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
    }
}
