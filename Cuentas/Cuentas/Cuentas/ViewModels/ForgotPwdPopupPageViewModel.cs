﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Prism.Navigation;
using Prism.Services;

namespace Cuentas.ViewModels
{
	public class ForgotPwdPopupPageViewModel : BaseViewModel
	{
	    private readonly IPageDialogService _pageDialogService;
	    private readonly IUsersApi _usersApi;

	    public ForgotPwdPopupPageViewModel(INavigationService navigationService,
	        IAccessTokenApiService accessTokenApiService, IPageDialogService pageDialogService, IUsersApi usersApi) : base(
	        navigationService, accessTokenApiService)
	    {
	        _usersApi = usersApi;
            
	        _pageDialogService = pageDialogService;
	    }

	    //Should update when ApiService is added to ViewModel 
	    private async Task CheckAccessToken()
	    {
	        await ValidationOAuthToken();
	        string accessToken = Configuration.Default.AccessToken;
	        _usersApi.Configuration.AccessToken = accessToken;
	    }

	    private string _email;
	    public string Email
	    {
	        get { return _email; }
	        set { SetProperty(ref _email, value); }
	    }

        private DelegateCommand _sendCommand;

	    public DelegateCommand SendCommand => _sendCommand ?? (_sendCommand =
	                                                new DelegateCommand(OnSendCommandExecuted, SendCommandCanExecute)
	                                                    .ObservesProperty(() => Email));

	    private async void OnSendCommandExecuted()
	    {
	        try
	        {
	            UserDialogs.Instance.ShowLoading("Loading...");
	            await _usersApi.SubmitPasswordResetAsync(new PasswordResetRequest(Email = Email));
	        }
	        catch (Exception ex)
	        {
	            await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
	        }
	        finally
	        {
	            UserDialogs.Instance.HideLoading();
	            await _navigationService.GoBackAsync();
	        }
	    }

	    private bool SendCommandCanExecute() => !string.IsNullOrWhiteSpace(Email) &&
	                                            Regex.IsMatch(Email, Constants.EmailRegex, RegexOptions.IgnoreCase) &&
	                                            IsNotBusy;
	}
}
