﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Model;
using Cuentas.Command;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Cuentas.ViewModels.PageModels;
using Cuentas.Views;
using Prism.Navigation;
using Xamarin.Forms;

namespace Cuentas.ViewModels
{
    public class FlyoutPageViewModel : BaseViewModel
    {
        #region Menu Constructor
        private readonly MenuGroupModel _myCardGroup = new MenuGroupModel("My Card")
        {
            new MenuModel("My Card", "Card"),
            new MenuModel("Home", "Home"),
            new MenuModel("Card Activity", "ActivityOfCard"),
            new MenuModel("Reload", "Reload"),
            new MenuModel("Reload Locations", "Locations"),
            new MenuModel("Recipients", "Recipients"),
            new MenuModel("Transfers", "Transfers")
        };

        private readonly MenuGroupModel _transfersGroup = new MenuGroupModel("Help")
        {
            new MenuModel("FAQs", $"WebView?id={Constants.FaqsId}"),
            new MenuModel("Contact", $"WebView?id={Constants.ContactId}"),
            new MenuModel("Help Wizard", $"WebView?id={Constants.HelpWizardId}")
        };

        private readonly MenuGroupModel _profileGroup = new MenuGroupModel("Profile")
        {
            new MenuModel("Alert Settings", "AlertSettings"),
            new MenuModel("Wallet Settings", "WalletSettings"),
            new MenuModel("Profile Settings", "ProfileSettings"),
            new MenuModel("Change Password", "ChangePassword")
        };

        private readonly MenuGroupModel _rewardsGroup = new MenuGroupModel("Rewards")
        {
            new MenuModel("How It Works", $"WebView?id={Constants.HowItWorksId}"),
            new MenuModel("Promo Page", $"WebView?id={Constants.PromoPageId}"),
            new MenuModel("Referral Program", "Referral"),
            new MenuModel("Legal", $"WebView?id={Constants.LegalId}")
        };

        private readonly MenuGroupModel _aboutGroup = new MenuGroupModel("About")
        {
            new MenuModel("Legal Page", $"WebView?id={Constants.LegalPageId}"),
            new MenuModel("Fees and Limits", $"WebView?id={Constants.FeesAndLimitsId}")
        };
        #endregion


        private readonly IUsersApi _usersApi;

        public FlyoutPageViewModel(INavigationService navigationService, IUsersApi userApi,
            IAccessTokenApiService accessTokenApiService) : base(navigationService, accessTokenApiService)
        {
            UserDialogs.Instance.HideLoading();
            User = Settings.Current.Get<UserResource>("CurrentUser");
            OAuth2Resource oauthToken = Settings.Current.Get<OAuth2Resource>("OAuthToken");
            userApi.Configuration.AccessToken = oauthToken.AccessToken;
            _usersApi = userApi;
            GroupedMenu = new ObservableCollection<MenuGroupModel>
            {
                _myCardGroup,
                _transfersGroup,
                _profileGroup,
                _rewardsGroup,
                _aboutGroup
            };
        }

        private ObservableCollection<MenuGroupModel> _groupedMenu;
        public ObservableCollection<MenuGroupModel> GroupedMenu
        {
            get { return _groupedMenu; }
            set { SetProperty(ref _groupedMenu, value); }
        }

        private MenuModel _selectedMenuItem;
        public MenuModel SelectedMenuItem
        {
            get { return _selectedMenuItem; }
            set { SetProperty(ref _selectedMenuItem, value); }
        }

        private UserResource _user;
        public UserResource User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private DelegateCommand _logoutCommand;
        public DelegateCommand LogoutCommand => _logoutCommand ?? (_logoutCommand =
                                                    new DelegateCommand(OnLogoutCommandExecuted));

        private void OnLogoutCommandExecuted()
        {
            if (Settings.Current.Remove("CurrentUser") && Settings.Current.Remove("OAuthToken"))
                Application.Current.MainPage = new NavigationPage(new LoginPage());
        }

        private DelegateCommand<MenuModel> _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand =
                                                    new DelegateCommand<MenuModel>(OnItemTappedCommandExecuted));

        private async void OnItemTappedCommandExecuted(MenuModel menuItem)
        {
            try
            {
                await _navigationService.NavigateAsync($"Navigation/{menuItem.Reference}");
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error: " + e);
                UserDialogs.Instance.ShowError("Page under construction.");
            }
        }
    }
}
