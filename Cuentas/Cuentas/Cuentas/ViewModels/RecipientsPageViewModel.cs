﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Models.AlertSettings;
using Cuentas.Models.BreEventNotification;
using Cuentas.Models.GeneralModels;
using Cuentas.Models.PendingRecipients;
using Cuentas.Models.PendingTransfers;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace Cuentas.ViewModels
{
    public class RecipientsPageViewModel : BaseViewModel
    {
        private readonly IPageDialogService _pageDialogService;
        private readonly IUsersGroupsApi _usersGroupsApi;
        private readonly IUsersApi _usersApi;
        private readonly IPaymentsWalletsApi _paymentsWalletsApi;
        private readonly IBRERuleEngineEventsApi _breRuleEngineEventsApi;
        private PropertyWrapper _propertyHelper;

        public RecipientsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IUsersApi usersApi, IBRERuleEngineEventsApi breRuleEngineEventsApi, IPaymentsWalletsApi paymentsWalletsApi, IAccessTokenApiService accessTokenApiService,
            IUsersGroupsApi usersGroupsApi) : base(navigationService, accessTokenApiService)
        {
            _usersApi = usersApi;
            _paymentsWalletsApi = paymentsWalletsApi;
            _breRuleEngineEventsApi = breRuleEngineEventsApi;
            _usersGroupsApi = usersGroupsApi;

            _pageDialogService = pageDialogService;
        }

        //Should update when ApiService is added to ViewModel 
        private async Task CheckAccessToken()
        {
            await ValidationOAuthToken();
            string accessToken = Configuration.Default.AccessToken;
            _usersApi.Configuration.AccessToken = accessToken;
            _paymentsWalletsApi.Configuration.AccessToken = accessToken;
            _breRuleEngineEventsApi.Configuration.AccessToken = accessToken;
            _usersGroupsApi.Configuration.AccessToken = accessToken;
        }

        private DelegateCommand<GroupMemberResource> _itemTappedCommand;
        public ICommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand =
                                                 new DelegateCommand<GroupMemberResource>(OnItemTappedCommandExecuted));

        private DelegateCommand _refreshCommand;
        public DelegateCommand RefreshCommand => _refreshCommand ?? (_refreshCommand =
                                                     new DelegateCommand(OnRefreshCommandExecuted));

        private DelegateCommand<GroupMemberResource> _sendMoneyCommand;
        public ICommand SendMoneyCommand => _sendMoneyCommand ?? (_sendMoneyCommand =
                                                     new DelegateCommand<GroupMemberResource>(OnSendMoneyCommandExecuted));

        private DelegateCommand _addCommand;
        public DelegateCommand AddCommand => _addCommand ?? (_addCommand =
                                                     new DelegateCommand(OnAddCommandExecuted));


        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set { SetProperty(ref _isRefreshing, value); }
        }

        private string _query;
        public string Query
        {
            get { return _query; }
            set { SetProperty(ref _query, value); }
        }

        private ObservableCollection<GroupMemberResource> _recipients;
        public ObservableCollection<GroupMemberResource> Recipients
        {
            get { return _recipients; }
            set { SetProperty(ref _recipients, value); }
        }

        private UserResource _user;
        public UserResource User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private void OnItemTappedCommandExecuted(GroupMemberResource member)
        {

        }

        #region It'll have worked by the time the server sends the correct list

        //private DelegateCommand _searchPressCommand;
        //public DelegateCommand SearchPressCommand => _searchPressCommand ?? (_searchPressCommand =
        //                                                 new DelegateCommand(OnSearchPressCommandExecuted));

        //private DelegateCommand _loadingMoreCommand;
        //public DelegateCommand LoadingMoreCommand => _loadingMoreCommand ?? (_loadingMoreCommand =
        //                                                 new DelegateCommand(OnLoadingMoreCommandExecuted, () => !IsLoading).ObservesProperty(() => IsLoading));

        //private bool _isLoading;
        //public bool IsLoading
        //{
        //    get { return _isLoading; }
        //    set { SetProperty(ref _isLoading, value); }
        //}

        //private async void OnLoadingMoreCommandExecuted()
        //{
        //    int offset = Recipients.Count;
        //    await SearchUsers(Query, offset);
        //}

        //private async void OnSearchPressCommandExecuted()
        //{
        //    IsBusy = true;
        //    Recipients?.Clear();
        //    int offset = Recipients.Count;
        //    await SearchUsers(Query, offset);
        //    IsBusy = false;
        //}
        #endregion

        private async void OnAddCommandExecuted()
        {
            var paramerter = new NavigationParameters {{"user", User}};
            await _navigationService.NavigateAsync("RequestSearch", paramerter);
        }

        private async void OnSendMoneyCommandExecuted(GroupMemberResource member)
        {
            try
            {
                var resultPrompt =  await UserDialogs.Instance.PromptAsync(new PromptConfig()
                .SetTitle("Send Money")
                .SetMessage($"To: {member.DisplayName}")
                .SetPlaceholder("Amount")
                .SetInputMode(InputType.DecimalNumber)
                .SetOkText("Send")
                .SetCancelText("Back"));
            
                if (resultPrompt.Ok && !string.IsNullOrWhiteSpace(resultPrompt.Text))
                {
                    await CheckAccessToken();

                    UserDialogs.Instance.ShowLoading("Loading...");
                    SimpleWallet responseWallet = await _paymentsWalletsApi.GetUserWalletAsync(User.Id, "USD");
                    decimal amount = Math.Abs(Convert.ToDecimal(resultPrompt.Text));
                    if (Math.Abs(amount) <= responseWallet.Balance)
                    {
                        _propertyHelper = new PropertyWrapper();
                        Transfer currentTransfer = new Transfer
                        {
                            CreationDate = {Value = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds()},  //current date in milliseconds
                            Amount = {Value = Convert.ToDouble(resultPrompt.Text)},
                            From = {Value = User.Id ?? default(int)},
                            To = {Value = member.Id ?? default(int)},
                            State = {Value = StateOfTransactionEnum.pending}
                        };

                        UserResource senderUser =
                            _propertyHelper.CreateOrUpdatePendingTransfersList(User, currentTransfer);
                        UserResource recipientUser = await _usersApi.GetUserAsync(Convert.ToString(member.Id));
                        recipientUser =
                            _propertyHelper.CreateOrUpdatePendingTransfersList(recipientUser, currentTransfer);
                        if (senderUser != null && recipientUser != null)
                        {
                            await _usersApi.UpdateUserAsync(Convert.ToString(senderUser.Id), senderUser);
                            await _usersApi.UpdateUserAsync(Convert.ToString(recipientUser.Id), recipientUser);
                            await UpdateCurrentUser();

                            string smsTemplate =
                                $"{User.DisplayName} has requested a new transfer. Follow {Constants.KNETIK_JSPAI_URL}/transfer-request?sender={currentTransfer.From.Value}&date={DateTime.UtcNow} to complete this transaction.";
                            var parameters = new BreEventParams(currentTransfer.To.Value, Convert.ToString(StateOfTransactionEnum.request), smsTemplate)
                            {
                                TemplateVars = new TemplateVars()
                                {
                                    Date = DateTime.UtcNow,
                                    DisplayName = User.DisplayName,
                                    Host = Constants.KNETIK_JSPAI_URL,
                                    Sender = currentTransfer.From.Value
                                }
                            };
                            await _breRuleEngineEventsApi.SendBREEventAsync(new BreEvent("notify_user",
                                parameters)); //Send the transfer request to the recipient

                            UserDialogs.Instance.HideLoading();
                            UserDialogs.Instance.ShowSuccess("The request has been sent. Wait to accept the transfer");
                        }
                        else
                        {
                            UserDialogs.Instance.HideLoading();
                            UserDialogs.Instance.Toast("The current transfer has already existed",
                                TimeSpan.FromSeconds(2));
                        }
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.Toast("You don't have enough money to send", TimeSpan.FromSeconds(2));
                    }
                }
            }
            //catch (ApiException e)
            //{
            //    if (e.ErrorCode == 401)
            //    {
            //        OAuth2Resource oauth = await e.UpdateAccessToken();
            //        UpdateToken(oauth);
            //    }
            //}
            catch (Exception e)
            {
                UserDialogs.Instance.HideLoading();
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }

        private async void OnRefreshCommandExecuted()
        {
            IsRefreshing = true;
            await GroupRefresh();
            IsRefreshing = false;
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("message"))
                UserDialogs.Instance.Toast(parameters["message"].ToString(), TimeSpan.FromSeconds(2));
        }

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            await GroupRefresh();
        }

        private async Task UpdateCurrentUser()
        {
            await CheckAccessToken();
            User = await _usersApi.GetUserAsync("me");
            Settings.Current.Set("CurrentUser", User);
        }
       

        private PendingRepicients DeserializePendingRecipients(UserResource user)
        {
            JObject pendingRecipients = user.AdditionalProperties["pending_recipients"];
            PendingRepicients userRecipients = JsonConvert.DeserializeObject<PendingRepicients>(
                pendingRecipients.ToString(),
                new JsonSerializerSettings()
                {
                    DateParseHandling = DateParseHandling.None,
                    MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                });
            return userRecipients;
        }

        private StringValueType<string> DeserializeIdentifiers(GroupMemberResource groupMember)
        {
            Debug.WriteLine(JObject.FromObject(groupMember));
            JObject identifierProperty = groupMember.AdditionalProperties["identifier"];
            StringValueType<string> identifier = JsonConvert.DeserializeObject<StringValueType<string>>(
                identifierProperty.ToString(),
                new JsonSerializerSettings()
                {
                    DateParseHandling = DateParseHandling.None,
                    MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                });
            return identifier;
        }

        private async Task GroupRefresh()
        {
            try
            {
                IsBusy = true;
                await UpdateCurrentUser();
                var resourceGroupMembers = await _usersGroupsApi.GetGroupMembersAsync($"{User.Id}-recipients");

                if (resourceGroupMembers != null && resourceGroupMembers.Content.Count != 0)
                {
                    resourceGroupMembers.Content.RemoveAll(resource =>
                        resource.Status == GroupMemberResource.StatusEnum.Moderator);
                    Recipients = new ObservableCollection<GroupMemberResource>(resourceGroupMembers.Content);
                    if (User.AdditionalProperties != null &&
                        User.AdditionalProperties.ContainsKey("pending_recipients"))
                    {
                        PendingRepicients recipients = DeserializePendingRecipients(User);
                        foreach (var member in resourceGroupMembers.Content)
                        {
                            if (member.AdditionalProperties != null &&
                                member.AdditionalProperties.ContainsKey("identifier"))
                            {
                                recipients.RecipientRequests.RemoveAll(mapRecipients =>
                                    new StringValueTypeCompare().Equals(DeserializeIdentifiers(member),
                                        mapRecipients.Recipient.MobilePhone));
                            }
                        }
                        JObject pendingRecipientsObject = JObject.FromObject(recipients);
                        User.AdditionalProperties.Remove("pending_recipients");
                        User.AdditionalProperties.Add("pending_recipients", pendingRecipientsObject);
                      
                        await _usersApi.UpdateUserAsync("me", User);
                        await UpdateCurrentUser();
                    }
                }
                else
                {
                    Recipients = new ObservableCollection<GroupMemberResource>();
                }
            }
            //catch (ApiException e)
            //{
            //    if (e.ErrorCode == 401)
            //    {
            //        OAuth2Resource oauth = await e.UpdateAccessToken();
            //        UpdateToken(oauth);
            //    }
            //}
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
            finally
            {
                IsBusy = false;
            }
        }
        
        //private void AddMember()
        //{

        //    //Add the current user to the new group (which had to be created before)
        //    GroupRepository.Instance.AddMemberToGroup($"{User.userId}-Recipients",
        //        new GroupMemberResource() {userId = User.userId, status = "member"},
        //        AddMemberToGroupHandler);
        //}

        //private void AddMemberToGroupHandler(GroupMemberResource result, string errorMessage, bool isSuccess)
        //{
        //    if (isSuccess)
        //    {
        //        GroupRefresh();
        //    }
        //    else
        //    {
        //        _pageDialogService.DisplayAlertAsync("Error", errorMessage, "OK");
        //    }
        //}
    }
}
