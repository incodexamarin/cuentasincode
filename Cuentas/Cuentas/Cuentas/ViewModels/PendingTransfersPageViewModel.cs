﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Models.BreEventNotification;
using Cuentas.Models.GeneralModels;
using Cuentas.Models.PendingTransfers;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Cuentas.ViewModels.PageModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using Prism.Services;

namespace Cuentas.ViewModels
{
	public class PendingTransfersPageViewModel : BaseViewModel
	{
	    private readonly IUsersApi _usersApi;
	    private readonly IPaymentsWalletsApi _paymentsWalletsApi;
	    private readonly IBRERuleEngineEventsApi _breRuleEngineEventsApi;
        private readonly IPageDialogService _pageDialogService;
	    public PendingTransfersPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService, IPageDialogService pageDialogService, IBRERuleEngineEventsApi breRuleEngineEventsApi,
	        IUsersApi usersApi, IPaymentsWalletsApi paymentsWalletsApi) : base(navigationService, accessTokenApiService)
	    {
            _usersApi = usersApi;
	        _breRuleEngineEventsApi = breRuleEngineEventsApi;
	        _paymentsWalletsApi = paymentsWalletsApi;

            _pageDialogService = pageDialogService;
	    }

	    //Should update when ApiService is added to ViewModel 
	    private async Task CheckAccessToken()
	    {
	        await ValidationOAuthToken();
	        string accessToken = Configuration.Default.AccessToken;
	        _usersApi.Configuration.AccessToken = accessToken;
	        _paymentsWalletsApi.Configuration.AccessToken = accessToken;
	        _breRuleEngineEventsApi.Configuration.AccessToken = accessToken;
	    }

        private DelegateCommand<MapTransfer> _rejectTransferCommand;
	    public ICommand RejectTransferCommand => _rejectTransferCommand ?? (_rejectTransferCommand =
	                                             new DelegateCommand<MapTransfer>(OnRejectTransferCommandExecuted));

	    private DelegateCommand<MapTransfer> _acceptTransferCommand;
	    public ICommand AcceptTransferCommand => _acceptTransferCommand ?? (_acceptTransferCommand =
	                                                 new DelegateCommand<MapTransfer>(OnAcceptTransferCommandExecuted));

	    private DelegateCommand _refreshCommand;
	    public DelegateCommand RefreshCommand => _refreshCommand ?? (_refreshCommand =
	                                                 new DelegateCommand(OnRefreshCommandExecuted));

        private UserResource _user;
	    public UserResource User
	    {
	        get { return _user; }
	        set { SetProperty(ref _user, value); }
	    }
	    private bool _isRefreshing;
	    public bool IsRefreshing
	    {
	        get { return _isRefreshing; }
	        set { SetProperty(ref _isRefreshing, value); }
	    }

        private ObservableCollection<MapTransfer> _pendingTransfers;
	    public ObservableCollection<MapTransfer> PendingTransfers
        {
	        get { return _pendingTransfers; }
	        set { SetProperty(ref _pendingTransfers, value); }
	    }

	    private async void OnRejectTransferCommandExecuted(MapTransfer transfer)
	    {
	        try
	        {
	            IsBusy = true;
                UserDialogs.Instance.ShowLoading("Loading...");
	            await CheckAccessToken();
	          
	            string date = DateTimeStringFromUnix(transfer.Transfer.CreationDate.Value);
                UserResource senderUser = await _usersApi.GetUserAsync(Convert.ToString(transfer.Transfer.From.Value));
	            PendingTransfers senderTransfers = DeserializePendingTransfers(senderUser);
	            var indexOfSender = senderTransfers.Transfers.IndexOf(
	                senderTransfers.Transfers.FirstOrDefault(mapTransfer =>
	                    new TransferCompare().Equals(mapTransfer.Transfer, transfer.Transfer)));
                var indexOfReceiver = PendingTransfers.IndexOf(transfer);
	            if (indexOfReceiver >= 0 && indexOfSender >= 0)
	            {
	                senderTransfers.Transfers.RemoveAt(indexOfSender);
	                PendingTransfers.RemoveAt(indexOfReceiver);

	                transfer.Transfer.State.Value = StateOfTransactionEnum.declined;
	                senderTransfers.Transfers.Add(transfer);
	                PendingTransfers.Add(transfer);
	                JObject pendingTransfersObject = JObject.FromObject(senderTransfers);
	                senderUser.AdditionalProperties["pending_transfers"] = pendingTransfersObject;
	                pendingTransfersObject =
	                    JObject.FromObject(new PendingTransfers() {Transfers = PendingTransfers.ToList()});
	                User.AdditionalProperties["pending_transfers"] = pendingTransfersObject;
	                await UpdateUsers(senderUser, User);
	                string smsTemplate = $"{User.DisplayName} has declined the transfer request created on {date}";
	                ;
	                await SendNotification(new TemplateVars {Date = DateTime.UtcNow, DisplayName = User.DisplayName},
	                    transfer.Transfer.From.Value, Convert.ToString(StateOfTransactionEnum.declined), smsTemplate);
	            }
	        }
	        //catch (ApiException e)
	        //{
	        //    if (e.ErrorCode == 401)
	        //    {
	        //        OAuth2Resource oauth = await e.UpdateAccessToken();
	        //        UpdateToken(oauth);
	        //    }
	        //}
            catch (Exception ex)
	        {
	            await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
	        }
	        finally
	        {
	            IsBusy = false;
	            UserDialogs.Instance.HideLoading();
	        }

	    }

	    private async void OnAcceptTransferCommandExecuted(MapTransfer transfer)
	    {
	        try
	        {
	            IsBusy = true;
	            UserDialogs.Instance.ShowLoading("Loading...");
                await CheckAccessToken();

                string date = DateTimeStringFromUnix(transfer.Transfer.CreationDate.Value);
                //TODO: Incapsulate and use abstraction for models
                #region Need to refactor this way
                string smsTemplate = $"{User.DisplayName} has accepted the transfer request created on {date}";
	            await SendNotification(new TemplateVars {Date = DateTime.UtcNow, DisplayName = User.DisplayName},
	                transfer.Transfer.From.Value, Convert.ToString(StateOfTransactionEnum.accepted), smsTemplate);

	            SimpleWallet responseWallet =
	                await _paymentsWalletsApi.GetUserWalletAsync(transfer.Transfer.From.Value, "USD");
	            if (Convert.ToDecimal(transfer.Transfer.Amount.Value) <= responseWallet.Balance)
	            {
	                UserResource senderUser = await _usersApi.GetUserAsync(Convert.ToString(transfer.Transfer.From.Value));
	                PendingTransfers senderTransfers = DeserializePendingTransfers(senderUser);

	                var indexOfSender = senderTransfers.Transfers.IndexOf(
	                    senderTransfers.Transfers.FirstOrDefault(mapTransfer =>
	                        new TransferCompare().Equals(mapTransfer.Transfer, transfer.Transfer)));
	                var indexOfReceiver = PendingTransfers.IndexOf(transfer);
	                if (indexOfReceiver >= 0 && indexOfSender >= 0 && senderUser.AdditionalProperties.ContainsKey("pending_transfers") && User.AdditionalProperties.ContainsKey("pending_transfers"))
	                {
	                    senderTransfers.Transfers.RemoveAt(indexOfSender);
	                    PendingTransfers.RemoveAt(indexOfReceiver);

	                    transfer.Transfer.State.Value = StateOfTransactionEnum.accepted;
	                    senderTransfers.Transfers.Add(transfer);
	                    PendingTransfers.Add(transfer);

                        JObject pendingTransfersObject = JObject.FromObject(senderTransfers);
	                    senderUser.AdditionalProperties["pending_transfers"] = pendingTransfersObject;
	                    pendingTransfersObject =
	                        JObject.FromObject(new PendingTransfers() {Transfers = PendingTransfers.ToList()});
	                    User.AdditionalProperties["pending_transfers"] = pendingTransfersObject;

	                    await UpdateUsers(senderUser, User);

	                    await _paymentsWalletsApi.UpdateWalletBalanceAsync(transfer.Transfer.From.Value, "USD",
	                        new WalletAlterRequest(-transfer.Transfer.Amount.Value, 0, $"Transfer: {senderUser.Username} to {User.Username}",
	                            "e-transaction"));
	                    await _paymentsWalletsApi.UpdateWalletBalanceAsync(transfer.Transfer.To.Value, "USD",
	                        new WalletAlterRequest(transfer.Transfer.Amount.Value, 0, $"Transfer: {senderUser.Username} to {User.Username}",
	                            "e-transaction"));

	                    smsTemplate = "There has been a p2p payment in your cuentas account, login for details.";
	                    await SendNotification(null, transfer.Transfer.From.Value,
	                        Convert.ToString(StateOfTransactionEnum.complete), smsTemplate);
	                    await SendNotification(null, transfer.Transfer.To.Value,
	                        Convert.ToString(StateOfTransactionEnum.complete), smsTemplate);

	                    UserDialogs.Instance.HideLoading();
	                    UserDialogs.Instance.ShowSuccess("Completed");
	                }
	            }
	            else
	            {
	                UserResource senderUser = await _usersApi.GetUserAsync(Convert.ToString(transfer.Transfer.From.Value));
	                PendingTransfers senderTransfers = DeserializePendingTransfers(senderUser);
	                var indexOfSender = senderTransfers.Transfers.IndexOf(
	                    senderTransfers.Transfers.FirstOrDefault(mapTransfer =>
	                        new TransferCompare().Equals(mapTransfer.Transfer, transfer.Transfer)));
                    var indexOfReceiver = PendingTransfers.IndexOf(transfer);
	                if (indexOfReceiver >= 0 && indexOfSender >= 0)
	                {
	                    senderTransfers.Transfers.RemoveAt(indexOfSender);
	                    PendingTransfers.RemoveAt(indexOfReceiver);

	                    transfer.Transfer.State.Value = StateOfTransactionEnum.failed;
                        senderTransfers.Transfers.Add(transfer);
                        PendingTransfers.Add(transfer);
	                    JObject pendingTransfersObject = JObject.FromObject(senderTransfers);
	                    senderUser.AdditionalProperties["pending_transfers"] = pendingTransfersObject;
	                    pendingTransfersObject =
	                        JObject.FromObject(new PendingTransfers() {Transfers = PendingTransfers.ToList()});
	                    User.AdditionalProperties["pending_transfers"] = pendingTransfersObject;
	                    await UpdateUsers(senderUser, User);
	                    smsTemplate = $"The transfer request created on {date} failed due to insufficent funds.";
	                    await SendNotification(new TemplateVars {Date = DateTime.UtcNow, DisplayName = User.DisplayName},
	                        transfer.Transfer.From.Value, Convert.ToString(StateOfTransactionEnum.failed), smsTemplate);


	                }
	                UserDialogs.Instance.HideLoading();
	                UserDialogs.Instance.Toast("Not enough money to send", TimeSpan.FromSeconds(2));
	            }
            #endregion
            }
	        //catch (ApiException e)
	        //{
	        //    if (e.ErrorCode == 401)
	        //    {
	        //        OAuth2Resource oauth = await e.UpdateAccessToken();
	        //        UpdateToken(oauth);
	        //    }
         //   }
            catch (Exception ex)
	        {
	            await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
	        }
	        finally
	        {
	            IsBusy = false;
	            UserDialogs.Instance.HideLoading();
	        }
	    }

	    public override async void OnNavigatedTo(NavigationParameters parameters)
        {
            await TransfersRefresh();
        }

	    private async void OnRefreshCommandExecuted()
	    {
	        IsRefreshing = true;
	        await TransfersRefresh();
	        IsRefreshing = false;
	    }

        private async Task TransfersRefresh()
	    {
	        try
	        {
	            IsBusy = true;
	            await CheckAccessToken();

                User = await _usersApi.GetUserAsync("me");
                Settings.Current.Set("CurrentUser", User);
                if (User.AdditionalProperties !=null && User.AdditionalProperties.ContainsKey("pending_transfers"))
	            {
	                var transfers = DeserializePendingTransfers(User);
                    PendingTransfers =
	                    new ObservableCollection<MapTransfer>(
	                        transfers.Transfers.FindAll(transfer => transfer.Transfer.To.Value == User.Id && transfer.Transfer.State.Value == StateOfTransactionEnum.pending));
	            }
	            else
	            {
	                PendingTransfers = new ObservableCollection<MapTransfer>();
	            }
	        }
            //catch (ApiException e)
            //{
            //    if (e.ErrorCode == 401)
            //    {
            //        OAuth2Resource oauth = await e.UpdateAccessToken();
            //        UpdateToken(oauth);
            //    }
            //}
            catch (Exception ex)
	        {
	            await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
	        }
	        finally
	        {
	            IsBusy = false;
	        }
	    }

	    private async Task UpdateUsers(UserResource senderUser, UserResource recipientUser)
	    {
            await _usersApi.UpdateUserAsync(Convert.ToString(senderUser.Id), senderUser);
	        await _usersApi.UpdateUserAsync(Convert.ToString(recipientUser.Id), recipientUser);
	        await TransfersRefresh();

	    }

	    private string DateTimeStringFromUnix(long milliseconds)
	    {
	        DateTimeOffset dateTimeOffset =
	            DateTimeOffset.FromUnixTimeMilliseconds(milliseconds);
	        DateTime dateTime = dateTimeOffset.UtcDateTime;
	        return dateTime.ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
        }

	    private PendingTransfers DeserializePendingTransfers(UserResource user)
	    {
	        JObject pendingTransfers = user.AdditionalProperties["pending_transfers"];
	        PendingTransfers userTransfers = JsonConvert.DeserializeObject<PendingTransfers>(
	            pendingTransfers.ToString(),
	            new JsonSerializerSettings()
	            {
	                DateParseHandling = DateParseHandling.None,
	                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
	                NullValueHandling = NullValueHandling.Ignore
	            });
	        return userTransfers;
	    }

	    private async Task SendNotification(TemplateVars templateVars, int? receiverId, string type, string smsTemplate)
	    {
	        BreEventParams parameters = new BreEventParams(receiverId, type, smsTemplate)
	        {
	            TemplateVars = templateVars
	        };
	        await _breRuleEngineEventsApi.SendBREEventAsync(new BreEvent("notify_user",
	            parameters)); //Send the transfer request to the recipient
	    }
	}
}
