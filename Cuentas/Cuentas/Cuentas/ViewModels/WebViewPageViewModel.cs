﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace Cuentas.ViewModels
{
	public class WebViewPageViewModel : BaseViewModel
	{
	    private readonly IContentArticlesApi _contentArticlesApi;
	    private readonly IPageDialogService _pageDialogService;
	    private string _htmlId;

	    public WebViewPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService,
	        IContentArticlesApi contentArticlesApi, IPageDialogService pageDialogService) : base(navigationService,
	        accessTokenApiService)
	    {
	        _contentArticlesApi = contentArticlesApi;
	        _pageDialogService = pageDialogService;
	    }

	    //Should update when ApiService is added to ViewModel 
	    private async Task CheckAccessToken()
	    {
	        await ValidationOAuthToken();
	        string accessToken = Configuration.Default.AccessToken;
	        _contentArticlesApi.Configuration.AccessToken = accessToken;
	    }

        private DelegateCommand _refreshCommand;
	    public DelegateCommand RefreshCommand => _refreshCommand ?? (_refreshCommand =
	                                                 new DelegateCommand(OnRefreshCommandExecuted));

	    private async void OnRefreshCommandExecuted()
	    {
	        IsRefreshing = true;
	        await GetHtmlPage(_htmlId);
            IsRefreshing = false;
	    }

        private HtmlWebViewSource _htmlSource;
	    public HtmlWebViewSource HtmlSource
	    {
	        get { return _htmlSource; }
	        set { SetProperty(ref _htmlSource, value); }
	    }

	    private string _titlePage;
	    public string TitlePage
	    {
	        get { return _titlePage; }
	        set { SetProperty(ref _titlePage, value); }
	    }

	    private bool _isRefreshing;
	    public bool IsRefreshing
	    {
	        get { return _isRefreshing; }
	        set { SetProperty(ref _isRefreshing, value); }
	    }

	    public override async void OnNavigatingTo(NavigationParameters parameters)
	    {
	        if (parameters.ContainsKey("id"))
	        {
	            _htmlId = (string) parameters["id"];
	            await GetHtmlPage(_htmlId);
	        }
	    }

	    private async Task GetHtmlPage(string id)
	    {
	        try
	        {
	            IsBusy = true;
	            if (!string.IsNullOrEmpty(id))
	            {
	                await CheckAccessToken();

                    ArticleResource htmlResult = await _contentArticlesApi.GetArticleAsync(id);
	                HtmlSource = new HtmlWebViewSource() {Html = htmlResult.Body};
	                TitlePage = htmlResult.Title;
	            }
	        }
	        //catch (ApiException e)
	        //{
	        //    if (e.ErrorCode == 401)
	        //    {
	        //        OAuth2Resource oauth = await e.UpdateAccessToken();
	        //        _contentArticlesApi.Configuration.AccessToken = oauth.AccessToken;
	        //    }

	        //}
            catch (Exception ex)
	        {
	            TitlePage = "Page";
	            UserDialogs.Instance.ShowError("This page haven't done yet.");
	        }
	        finally
	        {
	            IsRefreshing = false;
	            IsBusy = false;
	        }
	    }
	}
}
