﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Models;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Cuentas.Views;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using Prism.Services;

namespace Cuentas.ViewModels
{
    public class LoginPageViewModel : BaseViewModel
    {
        private readonly IPageDialogService _pageDialogService;
        private readonly IUsersApi _usersApi;

        public LoginPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService,
            IPageDialogService pageDialogService, IUsersApi usersApi) : base(navigationService, accessTokenApiService)
        {
            _usersApi = usersApi;
            _pageDialogService = pageDialogService;
            Email = "illiamelnykov@gmail.com";
            Password = "123123";
        }

        private DelegateCommand _loginCommand;

        public DelegateCommand LoginCommand => _loginCommand ?? (_loginCommand =
                                                   new DelegateCommand(OnLoginCommandExecuted, LoginCommandCanExecute)
                                                       .ObservesProperty(() => Email)
                                                       .ObservesProperty(() => Password));

        private DelegateCommand _signUpCommand;

        public DelegateCommand SignUpCommand => _signUpCommand ?? (_signUpCommand =
                                                    new DelegateCommand(OnSignUpCommandExecuted));

        private DelegateCommand _forgotPasswordCommand;

        public DelegateCommand ForgotPasswordCommand => _forgotPasswordCommand ?? (_forgotPasswordCommand =
                                                    new DelegateCommand(OnForgotPasswordCommandExecuted));

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        //public override async void OnNavigatingTo(NavigationParameters parameters)
        //{
        //    try
        //    {
        //        OAuth2Resource oauthToken = Settings.Current.Get<OAuth2Resource>("OAuthToken");
        //        UserResource user = Settings.Current.Get<UserResource>("CurrentUser");
        //        if (oauthToken != null && user != null)
        //        {
        //            UserDialogs.Instance.ShowLoading("Loading...");
        //            _usersApi.Configuration.AccessToken = oauthToken.AccessToken;
        //            user = await _usersApi.GetUserAsync("me");
        //            Settings.Current.Set("CurrentUser", user);
        //            await _navigationService.NavigateAsync("Index/Navigation/Home");
        //        }
        //    }
        //    catch (ApiException e)
        //    {
        //        if (e.ErrorCode == 401)
        //            await e.UpdateAccessToken();
        //    }
        //    catch (Exception exception)
        //    {
        //        await _pageDialogService.DisplayAlertAsync("Error", exception.Message, "OK");
        //    }
        //    finally
        //    {
        //        UserDialogs.Instance.HideLoading();
        //    }
        //}

        private async void OnLoginCommandExecuted()
        {
            try
            {
                UserDialogs.Instance.ShowLoading("Loading...");

                OAuth2Resource result = await _accessTokenApiService.GetOAuthTokenAsync(
                    Constants.GrandTypes.password.ToString(), Constants.ClIENT_ID, "", Email, Password, "", "");
                OAuthEntity response = new OAuthEntity(result, DateTime.UtcNow);
                Settings.Current.Set("OAuthToken", response);
               
                Configuration.Default.AccessToken = result.AccessToken;

                UserResource user = await _usersApi.GetUserAsync("me");
                Settings.Current.Set("CurrentUser", user);
                await _navigationService.NavigateAsync("Index/Navigation/Home");
            }
            //catch (ApiException e)
            //{
            //    if (e.ErrorCode == 401)
            //        e.UpdateAccessToken();
            //}
            catch (Exception exception)
            {
                await _pageDialogService.DisplayAlertAsync("Error", exception.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        private async void OnForgotPasswordCommandExecuted()
        {
            await _navigationService.NavigateAsync("ForgotPassword");
        }

        private async void OnSignUpCommandExecuted()
        {
            await _navigationService.NavigateAsync("Registration");
        }

        private bool LoginCommandCanExecute() =>
            !string.IsNullOrWhiteSpace(Email) && !string.IsNullOrWhiteSpace(Password) && Regex.IsMatch(Email, Constants.EmailRegex, RegexOptions.IgnoreCase) && IsNotBusy;
    }
}

