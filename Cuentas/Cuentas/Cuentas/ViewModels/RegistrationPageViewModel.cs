﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Model;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Prism.Navigation;
using Prism.Services;

namespace Cuentas.ViewModels
{
    public class RegistrationPageViewModel : BaseViewModel
    {
        IPageDialogService _pageDialogService { get; }
        private readonly IUsersApi _usersApi;

        public RegistrationPageViewModel(INavigationService navigationService,
            IAccessTokenApiService accessTokenApiService, IPageDialogService pageDialogService, IUsersApi usersApi) :
            base(navigationService, accessTokenApiService)
        {
            _usersApi = usersApi;
            _pageDialogService = pageDialogService;
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private string _confirmPassword;
        public string ConfirmPassword
        {
            get { return _confirmPassword; }
            set { SetProperty(ref _confirmPassword, value); }
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set { SetProperty(ref _userName, value); }
        }
       
        private DelegateCommand _registerCommand;

        public DelegateCommand RegisterCommand => _registerCommand ?? (_registerCommand =
                                                          new DelegateCommand(OnRegisterCommandExecuted,
                                                                  RegisterCommandCanExecute)
                                                              .ObservesProperty(() => Email)
                                                              .ObservesProperty(() => Password)
                                                              .ObservesProperty(() => ConfirmPassword)
                                                              .ObservesProperty(() => UserName));

        private async void OnRegisterCommandExecuted()
        {
            try
            {
                //UserDialogs.Instance.Loading("Loading...");
                IsBusy = true;
                await _usersApi.RegisterUserAsync(new UserResource(Email: Email, Username: UserName,
                    Password: Password));
                await _navigationService.GoBackAsync();
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
            finally
            {
                IsBusy = false;
                UserDialogs.Instance.HideLoading();
            }
        }

        private bool RegisterCommandCanExecute() =>
            !string.IsNullOrWhiteSpace(Email) && !string.IsNullOrWhiteSpace(Password) &&
            !string.IsNullOrWhiteSpace(UserName) && !string.IsNullOrWhiteSpace(ConfirmPassword) &&
            Regex.IsMatch(Email, Constants.EmailRegex, RegexOptions.IgnoreCase) && Password.Equals(ConfirmPassword) &&
            IsNotBusy;
    }
}
