﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Cuentas.Services.Abstractions;
using Prism.Navigation;

namespace Cuentas.ViewModels
{
    public class InviteSuccessPopupPageViewModel : BaseViewModel
    {
        public InviteSuccessPopupPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService) : base(navigationService, accessTokenApiService)
        {
        }

        private DelegateCommand _backCommand;

        public DelegateCommand BackCommand =>
            _backCommand ?? (_backCommand =
                new DelegateCommand(OnBackCommandExecuted));

        private DelegateCommand _backgroudClickCommand;

        public DelegateCommand BackgroudClickCommand =>
            _backgroudClickCommand ?? (_backgroudClickCommand =
                new DelegateCommand(OnBackCommandExecuted));

        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("destination"))
                Destination = (string)parameters["destination"];
        }

        private string _destination;
        public string Destination
        {
            get { return _destination; }
            set { SetProperty(ref _destination, value); }
        }

        private async void OnBackCommandExecuted()
        {
            NavigationParameters parameters = new NavigationParameters { { "invite", true } }; //this parameter indicate if Email/Phone field needs cleaning
            await _navigationService.GoBackAsync(parameters);
        }
    }
}
