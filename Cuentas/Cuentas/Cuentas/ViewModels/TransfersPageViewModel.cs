﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Cuentas.Services.Abstractions;
using Prism.Navigation;

namespace Cuentas.ViewModels
{
	public class TransfersPageViewModel : BaseViewModel
	{
	    public TransfersPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService) :
	        base(navigationService, accessTokenApiService)
	    {

	    }

	}
}
