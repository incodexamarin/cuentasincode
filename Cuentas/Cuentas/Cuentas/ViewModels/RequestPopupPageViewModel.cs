﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Models.GeneralModels;
using Cuentas.Models.PendingRecipients;
using Cuentas.Services;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using Prism.Services;
using Notification = Plugin.Notifications.Notification;

namespace Cuentas.ViewModels
{
    public class RequestPopupPageViewModel : BaseViewModel
    {
        private readonly IPageDialogService _pageDialogService;
        private readonly IExternalApiService _externalApiService;
        private readonly IUsersApi _usersApi;

        public RequestPopupPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IAccessTokenApiService accessTokenApiService, IUsersApi usersApi, IExternalApiService externalApiService) : base(navigationService, accessTokenApiService)
        {
            _usersApi = usersApi;
            _externalApiService = externalApiService;

            _pageDialogService = pageDialogService;
        }

        //Should update when ApiService is added to ViewModel 
        private async Task CheckAccessToken()
        {
            await ValidationOAuthToken();
            string accessToken = Configuration.Default.AccessToken;
            _usersApi.Configuration.AccessToken = accessToken;
        }

        private DelegateCommand _searchCommand;

        public DelegateCommand SearchCommand => _searchCommand ?? (_searchCommand =
                                                    new DelegateCommand(OnSearchCommandExecuted, SearchCommandCanExecute)
                                                        .ObservesProperty(() => Phone)
                                                        .ObservesProperty(() => Email));


        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("user"))
                User = (UserResource) parameters["user"];
        }

        private UserResource _user;
        public UserResource User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { SetProperty(ref _phone, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private async void OnSearchCommandExecuted()
        {
            NavigationParameters parameter = new NavigationParameters();
            try
            {
                await CheckAccessToken();

                #region SMS request to invite the user
                UserDialogs.Instance.ShowLoading("Loading...");
                if (!String.IsNullOrEmpty(Phone))
                {
                    RecipientRequest recipientRequest = new RecipientRequest()
                    {
                        MobilePhone = { Value = Phone },
                        Kind = { Value = KindSearchRecipientsEnum.mobile_number }
                    };
                    parameter = await HandleRequest(recipientRequest, parameter);
                }
                #endregion

                #region Email request to invite the user
                if (!String.IsNullOrEmpty(Email))
                {
                    RecipientRequest recipientRequest = new RecipientRequest()
                    {
                        Email = { Value = Email },
                        Kind = { Value = KindSearchRecipientsEnum.email }
                    };
                    parameter = await HandleRequest(recipientRequest, parameter);
                }
                #endregion
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
                await _navigationService.GoBackAsync(parameter);
            }
        }

        private async Task<NavigationParameters> HandleRequest(RecipientRequest recipientRequest,
            NavigationParameters parameters)
        {

            PropertyWrapper propertyHelper = new PropertyWrapper();
            UserResource senderUser =
                propertyHelper.CreateOrUpdatePendingRecipientsList(User, recipientRequest);
            if (senderUser != null)

            {
                await _usersApi.UpdateUserAsync(Convert.ToString(senderUser.Id), senderUser);
                await UpdateCurrentUser();
                Debug.WriteLine("-----Add to the pending list-----");
                Debug.WriteLine(JObject.FromObject(senderUser).ToString());
                await _externalApiService.PostInviteRecipientAsync(new InviteRequest
                {
                    Recipient = recipientRequest.Email.Value ?? recipientRequest.MobilePhone.Value,
                    Type = recipientRequest.Kind.Value
                });
            }
            parameters.Add("message", "The request has been sent.");

            return parameters;
        }

        private async Task UpdateCurrentUser()
        {
            User = await _usersApi.GetUserAsync("me");
            Settings.Current.Set("CurrentUser", User);
        }

        private bool SearchCommandCanExecute() =>
            !string.IsNullOrWhiteSpace(Phone) ^ !string.IsNullOrEmpty(Email); //&& Regex.IsMatch(Email, Constants.EmailRegex, RegexOptions.IgnoreCase)) && IsNotBusy;
    }
}
