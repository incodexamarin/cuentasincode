﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Model;
using Cuentas.Services;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Newtonsoft.Json.Linq;
using Prism.Events;
using Prism.Navigation;
using Prism.Services;
using Xamarin.Forms;

namespace Cuentas.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {
        private readonly IPageDialogService _pageDialogService;
        private readonly IExternalApiService _externalApiService;

        private bool _isViewed = false;

        public HomePageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService,
            IPageDialogService pageDialogService, IExternalApiService externalApiService) : base(navigationService,
            accessTokenApiService)
        {
            _externalApiService = externalApiService;
            _pageDialogService = pageDialogService;
        }


        //TODO: Should add the activity indicator
        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            HandleInvite();
        }

        public override void OnAppearing()
        {

        }

        public override async void OnResume()
        {
            await HandleInvite();
        }

        private async Task HandleInvite()
        {
            try
            {
                string uuid = Settings.Current.Get<string>("InviteToken");
                if (!string.IsNullOrEmpty(uuid) && !_isViewed)
                {
                    //_isViewed = true;
                    var fakeUser = Settings.Current.Get<UserResource>("CurrentUser"); //TODO: Need to change fake data
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        var result = await _pageDialogService.DisplayAlertAsync("Info",
                            $"{fakeUser.FirstName} {fakeUser.LastName} at {fakeUser.Email}" +
                            $" would like to add you as a transfer recipient? " +
                            $"Allow {fakeUser.FirstName} {fakeUser.LastName} them to submit transfer requests to you? " +
                            $"You can accept or reject each request, if \"Yes\" ",
                            "Accept", "Reject");

                        if (result)
                        {
                            UserDialogs.Instance.Loading("Loading...");
                            await _externalApiService.PostAcceptInviteAsync(uuid);
                            Settings.Current.Remove("InviteToken");
                            UserDialogs.Instance.ShowSuccess("You were added to the group");
                        }
                        else
                        {
                            Settings.Current.Remove("InviteToken");
                        }
                    });
                }
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
    }
}


