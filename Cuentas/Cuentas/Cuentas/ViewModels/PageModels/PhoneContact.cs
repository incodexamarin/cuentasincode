﻿namespace Cuentas.ViewModels.PageModels
{
    public class PhoneContact
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Contact { get; set; }

        public string Name => $"{FirstName} {LastName}";
    }
}