﻿namespace Cuentas.ViewModels.PageModels
{
    public class MenuModel
    {
        public string Title { get; }
        public string Reference { get; }

        public MenuModel(string title, string reference)
        {
            Title = title;
            Reference = reference;
        }
    }
}