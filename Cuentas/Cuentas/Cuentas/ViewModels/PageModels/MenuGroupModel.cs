﻿using System.Collections.ObjectModel;

namespace Cuentas.ViewModels.PageModels
{
    public class MenuGroupModel : ObservableCollection<MenuModel>
    {
        public string GroupTitle { get; }

        public MenuGroupModel(string title)
        {
            GroupTitle = title;
        }
    }
}