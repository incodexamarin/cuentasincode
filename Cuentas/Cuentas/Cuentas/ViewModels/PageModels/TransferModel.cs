﻿using Cuentas.Models.GeneralModels;

namespace Cuentas.ViewModels.PageModels
{
    public class TransferModel
    {
        public long CreationDate { get; set; }
        public int To { get; set; }
        public int From { get; set; }
        public double Amount { get; set; }
        public StateOfTransactionEnum State { get; set; }
    }
}