﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Acr.Settings;
using Acr.UserDialogs;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Cuentas.Models.AlertSettings;
using Cuentas.Services.Abstractions;
using Cuentas.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using Prism.Services;

namespace Cuentas.ViewModels
{
    public class AlertSettingsPageViewModel : BaseViewModel
    {
        private readonly IPageDialogService _pageDialogService;
        private readonly IUsersApi _usersApi;
        public AlertSettingsPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService, IPageDialogService pageDialogService, IUsersApi usersApi) :
            base(navigationService,accessTokenApiService)
        {
            _pageDialogService = pageDialogService;
            _usersApi = usersApi;
        }

        //Should update when ApiService is added to ViewModel 
        private async Task CheckAccessToken()
        {
            await ValidationOAuthToken();
            string accessToken = Configuration.Default.AccessToken;
            _usersApi.Configuration.AccessToken = accessToken;
        }

        private DelegateCommand _saveAlertSettingsCommand;
        public DelegateCommand SaveAlertSettingsCommand => _saveAlertSettingsCommand ?? (_saveAlertSettingsCommand =
                                                     new DelegateCommand(OnSaveAlertSettingsCommandExecuted));
       

        private UserResource _user;
        public UserResource User
        {
            get { return _user; }
            set { SetProperty(ref _user, value); }
        }

        private AlertSettings _alertSettings;
        public AlertSettings AlertSettings
        {
            get { return _alertSettings; }
            set { SetProperty(ref _alertSettings, value); }
        }

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            try
            {
                User = Settings.Current.Get<UserResource>("CurrentUser");
                AlertSettings = new AlertSettings();
                if (User.AdditionalProperties !=null && User.AdditionalProperties.ContainsKey("alert_settings"))
                {
                    JObject settings = User.AdditionalProperties["alert_settings"];
                    AlertSettings = JsonConvert.DeserializeObject<AlertSettings>(settings.ToString(),
                        new JsonSerializerSettings()
                        {
                            DateParseHandling = DateParseHandling.None,
                            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                            NullValueHandling = NullValueHandling.Ignore
                        });
                }
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
       
        private async void OnSaveAlertSettingsCommandExecuted()
        {
            try
            {
                await CheckAccessToken();

                UserDialogs.Instance.ShowLoading("Loading...");
                JObject settingsObject = JObject.FromObject(AlertSettings);
                if (User.AdditionalProperties == null)
                {
                    User.AdditionalProperties = new Dictionary<string, JObject> {{"alert_settings", settingsObject}};
                }
                else
                {
                    User.AdditionalProperties.Remove("alert_settings");
                    User.AdditionalProperties.Add("alert_settings", settingsObject);
                    JObject b = JObject.FromObject(User);
                    Debug.WriteLine(b.ToString());
                }
                await _usersApi.UpdateUserAsync(Convert.ToString(User.Id), User);
                User = await _usersApi.GetUserAsync("me");
                Settings.Current.Set("CurrentUser", User);
                UserDialogs.Instance.HideLoading();
            }
            //catch (ApiException e)
            //{
            //    if (e.ErrorCode == 401)
            //    {
            //        OAuth2Resource oauth = await e.UpdateAccessToken();
            //        _usersApi.Configuration.AccessToken = oauth.AccessToken;
            //    }
            //}
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
    }
}
