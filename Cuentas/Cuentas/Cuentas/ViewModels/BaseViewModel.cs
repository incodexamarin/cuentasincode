﻿using System.Threading.Tasks;
using Cuentas.Services.Abstractions;
using Prism.AppModel;
using Prism.Mvvm;
using Prism.Navigation;

namespace Cuentas.ViewModels
{
    public class BaseViewModel : BindableBase, INavigationAware, IApplicationLifecycleAware, IPageLifecycleAware
    {
        protected INavigationService _navigationService { get; }
        protected IAccessTokenApiService _accessTokenApiService { get; }
        public BaseViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService)
        {
            _navigationService = navigationService;
            _accessTokenApiService = accessTokenApiService;
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value, () => RaisePropertyChanged(nameof(IsNotBusy))); }
        }

        public bool IsNotBusy
        {
            get { return !IsBusy; }
        }

        protected async Task ValidationOAuthToken()
        {
            await _accessTokenApiService.ValidateTokenAsync();
        }

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {
        }

        public virtual void OnResume()
        {
        }

        public virtual void OnSleep()
        {
        }

        public virtual void OnAppearing()
        {
        }

        public virtual void OnDisappearing()
        {
        }
    }
}