﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using com.knetikcloud.Api;
using com.knetikcloud.Client;
using com.knetikcloud.Model;
using Acr.Settings;
using Cuentas.Models.GeneralModels;
using Cuentas.Models.PendingTransfers;
using Cuentas.Services.Abstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Prism.Navigation;
using Prism.Services;

namespace Cuentas.ViewModels
{
	public class CompleteTransfersListPageViewModel : BaseViewModel
	{
	    private readonly IUsersApi _usersApi;
	    private readonly IPageDialogService _pageDialogService;
        public CompleteTransfersListPageViewModel(INavigationService navigationService, IAccessTokenApiService accessTokenApiService, IPageDialogService pageDialogService, IUsersApi usersApi) : base(navigationService, accessTokenApiService)
        {
            _usersApi = usersApi;

            _pageDialogService = pageDialogService;
        }


	    //Should update when ApiService is added to ViewModel 
	    private async Task CheckAccessToken()
	    {
	        await ValidationOAuthToken();
	        string accessToken = Configuration.Default.AccessToken;
	        _usersApi.Configuration.AccessToken = accessToken;
        }

        private DelegateCommand _refreshCommand;
	    public DelegateCommand RefreshCommand => _refreshCommand ?? (_refreshCommand =
	                                                 new DelegateCommand(OnRefreshCommandExecuted));

	    private UserResource _user;
	    public UserResource User
	    {
	        get { return _user; }
	        set { SetProperty(ref _user, value); }
	    }
	    private bool _isRefreshing;
	    public bool IsRefreshing
	    {
	        get { return _isRefreshing; }
	        set { SetProperty(ref _isRefreshing, value); }
	    }

	    private ObservableCollection<MapTransfer> _pendingTransfers;
	    public ObservableCollection<MapTransfer> PendingTransfers
	    {
	        get { return _pendingTransfers; }
	        set { SetProperty(ref _pendingTransfers, value); }
	    }

	    public override async void OnAppearing()
	    {
	        await TransfersRefresh();
	    }

	    private async void OnRefreshCommandExecuted()
	    {
	        IsRefreshing = true;
	        await TransfersRefresh();
	        IsRefreshing = false;
	    }

	    private async Task TransfersRefresh()
	    {
	        try
	        {
	            IsBusy = true;
	            await CheckAccessToken();

	            User = await _usersApi.GetUserAsync("me");
	            Settings.Current.Set("CurrentUser", User);
	            if (User.AdditionalProperties != null && User.AdditionalProperties.ContainsKey("pending_transfers"))
	            {
	                var transfers = DeserializePendingTransfers(User);
	                PendingTransfers =
	                    new ObservableCollection<MapTransfer>(
	                        transfers.Transfers.FindAll(transfer => transfer.Transfer.State.Value != StateOfTransactionEnum.pending));
	            }
	            else
	            {
	                PendingTransfers = new ObservableCollection<MapTransfer>();
	            }
	        }
	        //catch (ApiException e)
	        //{
	        //    if (e.ErrorCode == 401)
	        //    {
	        //        OAuth2Resource oauth = await e.UpdateAccessToken();
	        //        UpdateToken(oauth);
	        //    }
	        //}
	        catch (Exception ex)
	        {
	            await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
	        }
	        finally
	        {
	            IsBusy = false;
	        }
	    }

	    private PendingTransfers DeserializePendingTransfers(UserResource user)
	    {
	        JObject pendingTransfers = user.AdditionalProperties["pending_transfers"];
	        PendingTransfers userTransfers = JsonConvert.DeserializeObject<PendingTransfers>(
	            pendingTransfers.ToString(),
	            new JsonSerializerSettings()
	            {
	                DateParseHandling = DateParseHandling.None,
	                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
	                NullValueHandling = NullValueHandling.Ignore
	            });
	        return userTransfers;
	    }
    }
}
