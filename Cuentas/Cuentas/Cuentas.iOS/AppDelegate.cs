﻿using System;
using Cuentas.iOS.Services;
using Cuentas.Services.Abstractions;
using Foundation;
using UIKit;
using Prism;
using Prism.Ioc;
using HockeyApp.iOS;
using Prism.Unity;
using Unity;

namespace Cuentas.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //


        private App _pclApplication;
        //Calculate size to show NativeListCells
        public static double DeviceWidth = UIScreen.MainScreen.Bounds.Width * 2;
        public static double DeviceHeight = UIScreen.MainScreen.Bounds.Height * 2;

        private const string HockeyappAppid = "5bc1583ee16d4cfab11ae0f101ed1f48";

        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            LoadApp();
            
            return base.FinishedLaunching(app, options);
        }

        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            // Call PCL method that will handle the OpenUrl using memorized instance
            if (!string.IsNullOrEmpty(url.Query)) _pclApplication.HandleUrl(url.Query);
            return true;
        }
      
        private void LoadApp()
        {
            //Get the shared instance
            var manager = BITHockeyManager.SharedHockeyManager;
            //Configure it to use our APP_ID
            manager.Configure(HockeyappAppid);
            manager.LogLevel = BITLogLevel.Debug;
            //Start the manager
            manager.StartManager();
            //Authenticate (there are other authentication options)
            manager.Authenticator.AuthenticateInstallation();

            Rg.Plugins.Popup.Popup.Init();
            global::Xamarin.Forms.Forms.Init();
            // Create and memorize the PCL application instance: it is the key
            _pclApplication = new App(new iOSInitializer());
            LoadApplication(_pclApplication);
        }
    }

    public class iOSInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterInstance(typeof(IContactService), new ContactService());
        }
    }
}
