﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Contacts;
using Foundation;
using Cuentas.Services.Abstractions;
using Cuentas.ViewModels.PageModels;

namespace Cuentas.iOS.Services
{
    public class ContactService : IContactService
    {
        private IEnumerable<CNContact> GetAllContacts()
        {
            var keysToFetch = new[] { CNContactKey.GivenName, CNContactKey.FamilyName, CNContactKey.PhoneNumbers, CNContactKey.EmailAddresses };
            NSError error;
            //var containerId = new CNContactStore().DefaultContainerIdentifier;
            // using the container id of null to get all containers.
            // If you want to get contacts for only a single container type, you can specify that here
            var contactList = new List<CNContact>();

            using (var store = new CNContactStore())
            {
                var allContainers = store.GetContainers(null, out error);
                CNAuthorizationStatus authStatus = CNContactStore.GetAuthorizationStatus(CNEntityType.Contacts);
                if (authStatus != CNAuthorizationStatus.Authorized) return null;

                foreach (var container in allContainers)
                {
                    try
                    {
                        using (var predicate = CNContact.GetPredicateForContactsInContainer(container.Identifier))
                        {
                            var containerResults = store.GetUnifiedContacts(predicate, keysToFetch, out error);
                            contactList.AddRange(containerResults);
                        }
                    }
                    catch
                    {
                        // ignore missed contacts from errors
                    }
                }
            }
            return contactList;
        }

        public IEnumerable<PhoneContact> GetContactsByPhone()
        {
            var contactList = GetAllContacts();
            var contacts = new List<PhoneContact>();
            if (contactList == null) return null;
            foreach (var item in contactList)
            {
                var numbers = item.PhoneNumbers;
                if (numbers != null)
                {
                    contacts.AddRange(numbers.Select(item2 => new PhoneContact
                    {
                        FirstName = item.GivenName,
                        LastName = item.FamilyName,
                        Contact = item2.Value.StringValue
                    }));
                }
            }
            return contacts;
        }

        public IEnumerable<PhoneContact> GetContactsByEmail()
        {
            var contactList = GetAllContacts();
            var contacts = new List<PhoneContact>();
            if (contactList == null) return null;
            foreach (var item in contactList)
            {
                var emails = item.EmailAddresses;
                if (emails != null)
                {
                    contacts.AddRange(emails.Select(item2 => new PhoneContact
                    {
                        FirstName = item.GivenName,
                        LastName = item.FamilyName,
                        Contact = item2.Value
                    }));
                }
            }
            return contacts;
        }
    }
}